﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
//using WinFormsLibrary;
using WinFormsLib;
namespace WinFormsApp
{
    public partial class KeeperForm : Form
    {
        public KeeperForm()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(4, 20, 29);
        }

        private void KeeperCardForm_Load(object sender, EventArgs e)
        {
            pictureBoxPlayer.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images\unknown.jpg"));
            lblName.Text = $"{DataSender.Keeper.Name} {DataSender.Keeper.Surname}";
            lblAge.Text = $"{(DateTime.Today - DataSender.Keeper.DateOfBirth).TotalDays / 365:F0}  ({DataSender.Keeper.DateOfBirth.Year}/{DataSender.Keeper.DateOfBirth.Month}/{DataSender.Keeper.DateOfBirth.Day})";


            pictureBoxGlove.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/glove.jpg"));
            lblCleanSheets.Text = $"- {DataSender.Keeper.CleanSheets}";
            pictureBoxConcededGoals.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/ballInTheNet.jpg"));
            lblConcededGoals.Text = $"- {DataSender.Keeper.GoalsConceded}";
            pictureBoxGoal.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/goal.png"));
            lblGoal.Text = $"- {DataSender.Keeper.Goals}";
            pictureBoxAssist.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/assist.jpg"));
            lblAssist.Text = $"- {DataSender.Keeper.Assists}";
            pictureBoxPenalty.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/penalty.jpg"));
            lblPenalty.Text = $"- {DataSender.Keeper.PenaltyGoals}";
            pictureBoxOwnGoal.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/ownGoal.jpg"));
            lblOwnGoal.Text = $"- {DataSender.Keeper.OwnGoals}";
            pictureBoxYellow.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/yellowCard.png"));
            lblYellow.Text = $"- {DataSender.Keeper.YellowCards}";
            pictureBoxRed.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/redCard.jpg"));
            lblRed.Text = $"- {DataSender.Keeper.RedCards}";
        }
    }
}
