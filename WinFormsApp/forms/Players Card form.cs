﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
//using WinFormsLibrary;
using WinFormsLib;
namespace WinFormsApp
{
    public partial class PlayersForm : Form
    {
        public PlayersForm()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(4, 20, 29);
        }

        private void PlayersForm_Load(object sender, EventArgs e)
        {
            pictureBoxPlayer.Image= new Bitmap(Path.Combine(Application.StartupPath, @"images\unknown.jpg"));
            lblName.Text = $"{DataSender.Player.Name} {DataSender.Player.Surname}";
            lblAge.Text = $"{(DateTime.Today - DataSender.Player.DateOfBirth).TotalDays / 365:F0}  ({DataSender.Player.DateOfBirth.Year}/{DataSender.Player.DateOfBirth.Month}/{DataSender.Player.DateOfBirth.Day})";

            pictureBoxGoal.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/goal.png"));
            lblGoal.Text = $"- {DataSender.Player.Goals}";
            pictureBoxAssist.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/assist.jpg"));
            lblAssist.Text = $"- {DataSender.Player.Assists}";
            pictureBoxPenalty.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/penalty.jpg"));
            lblPenalty.Text = $"- {DataSender.Player.PenaltyGoals}";
            pictureBoxOwnGoal.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/ownGoal.jpg"));
            lblOwnGoal.Text = $"- {DataSender.Player.OwnGoals}";
            pictureBoxYellow.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/yellowCard.png"));
            lblYellow.Text = $"- {DataSender.Player.YellowCards}";
            pictureBoxRed.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/redCard.jpg"));
            lblRed.Text = $"- {DataSender.Player.RedCards}";
        }
    }
}
