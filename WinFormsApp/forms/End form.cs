﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using WinFormsLibrary;
using WinFormsLib;
namespace WinFormsApp
{
    public partial class End_form : Form
    {
        public End_form()
        {
            InitializeComponent();
        }

        private void End_form_Load(object sender, EventArgs e)
        {
            lblFirst.Text = $"{DataSender.Teams[0].TeamName}";
            lblSecond.Text = $"{DataSender.Teams[1].TeamName}";
            lblThird.Text = $"{DataSender.Teams[2].TeamName}";

            List<string> names = new List<string>();
            int max=0;
            for(int i=0;i<DataSender.Teams.Length; i++)
            {
                int[] indexes;
                indexes = DataSender.Teams[i].GetTopScorers();
                if (DataSender.Teams[i].FieldPlayers[indexes[0]].Goals >= max) {
                    if(DataSender.Teams[i].FieldPlayers[indexes[0]].Goals > max)
                    {
                        max = DataSender.Teams[i].FieldPlayers[indexes[0]].Goals;
                        names.Clear();
                    }
                    foreach (int item in indexes)
                    {
                        names.Add(DataSender.Teams[i].FieldPlayers[item].Name + " " + DataSender.Teams[i].FieldPlayers[item].Surname + $"({max})");
                    }
                }
            }
            foreach (string item in names)
            {
                string comma = ", ";
                if (item == names[names.Count - 1]) comma = "";
                lblScorer.Text += item + comma;
            }
            names.Clear(); max = 0;
            for (int i = 0; i < DataSender.Teams.Length; i++)
            {
                int[] indexes;
                indexes = DataSender.Teams[i].GetTopAssistant();
                if (DataSender.Teams[i].FieldPlayers[indexes[0]].Assists >= max)
                {
                    if (DataSender.Teams[i].FieldPlayers[indexes[0]].Assists > max)
                    {
                        max = DataSender.Teams[i].FieldPlayers[indexes[0]].Assists;
                        names.Clear();
                    }
                    foreach (int item in indexes)
                    {
                        names.Add(DataSender.Teams[i].FieldPlayers[item].Name + " " + DataSender.Teams[i].FieldPlayers[item].Surname +$"({max})");
                    }
                }
            }
            foreach (string item in names)
            {
                string comma = ", ";
                if (item == names[names.Count - 1]) comma = "";
                lblAssistant.Text += item + comma;
            }
            names.Clear(); max = 0;
            for (int i = 0; i < DataSender.Teams.Length; i++)
            {
                int[] indexes;
                indexes = DataSender.Teams[i].GetTopGoalKeeper();
                if (DataSender.Teams[i].GoalKeepers[indexes[0]].CleanSheets >= max)
                {
                    if (DataSender.Teams[i].GoalKeepers[indexes[0]].CleanSheets > max)
                    {
                        max = DataSender.Teams[i].GoalKeepers[indexes[0]].CleanSheets;
                        names.Clear();
                    }
                    foreach (int item in indexes)
                    {
                        names.Add(DataSender.Teams[i].GoalKeepers[item].Name + " " + DataSender.Teams[i].GoalKeepers[item].Surname + $"({max})");
                    }
                }
            }
            foreach (string item in names)
            {
                string comma = ", ";
                if (item == names[names.Count - 1]) comma = "";
                lblKeeper.Text += item + comma;
            }
        }
    }
}
