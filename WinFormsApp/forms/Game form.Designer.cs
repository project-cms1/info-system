﻿namespace WinFormsApp
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxHt = new System.Windows.Forms.PictureBox();
            this.pictureBoxAt = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblHt = new System.Windows.Forms.Label();
            this.lblAt = new System.Windows.Forms.Label();
            this.lblScore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAt)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxHt
            // 
            this.pictureBoxHt.Location = new System.Drawing.Point(44, 35);
            this.pictureBoxHt.Name = "pictureBoxHt";
            this.pictureBoxHt.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxHt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHt.TabIndex = 0;
            this.pictureBoxHt.TabStop = false;
            // 
            // pictureBoxAt
            // 
            this.pictureBoxAt.Location = new System.Drawing.Point(401, 35);
            this.pictureBoxAt.Name = "pictureBoxAt";
            this.pictureBoxAt.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxAt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAt.TabIndex = 1;
            this.pictureBoxAt.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.panel1.Location = new System.Drawing.Point(270, 129);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 290);
            this.panel1.TabIndex = 2;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDate.Location = new System.Drawing.Point(203, 9);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(44, 18);
            this.lblDate.TabIndex = 3;
            this.lblDate.Text = "label1";
            // 
            // lblHt
            // 
            this.lblHt.AutoSize = true;
            this.lblHt.Font = new System.Drawing.Font("Palatino Linotype", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblHt.Location = new System.Drawing.Point(53, 105);
            this.lblHt.Name = "lblHt";
            this.lblHt.Size = new System.Drawing.Size(60, 24);
            this.lblHt.TabIndex = 4;
            this.lblHt.Text = "label1";
            // 
            // lblAt
            // 
            this.lblAt.AutoSize = true;
            this.lblAt.Font = new System.Drawing.Font("Palatino Linotype", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblAt.Location = new System.Drawing.Point(410, 105);
            this.lblAt.Name = "lblAt";
            this.lblAt.Size = new System.Drawing.Size(60, 24);
            this.lblAt.TabIndex = 5;
            this.lblAt.Text = "label2";
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Font = new System.Drawing.Font("Lucida Console", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblScore.Location = new System.Drawing.Point(229, 61);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(100, 24);
            this.lblScore.TabIndex = 6;
            this.lblScore.Text = "label1";
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(539, 461);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.lblAt);
            this.Controls.Add(this.lblHt);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBoxAt);
            this.Controls.Add(this.pictureBoxHt);
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Name = "GameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "game";
            this.Load += new System.EventHandler(this.GameForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PictureBox pictureBoxHt;
        private PictureBox pictureBoxAt;
        private Panel panel1;
        private Label lblDate;
        private Label lblHt;
        private Label lblAt;
        private Label lblScore;
    }
}