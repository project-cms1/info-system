﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using WinFormsLibrary;
using WinFormsLib;
namespace WinFormsApp
{
    public partial class GameForm : Form
    {
        public GameForm()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(8, 20, 40);
            lblHt.Text = DataSender.SelectedGame.HomeTeamName;
            lblAt.Text = DataSender.SelectedGame.AwayTeamName;
            lblDate.Text = DataSender.SelectedGame.MatchDay.ToString();
            pictureBoxHt.Image = DataSender.SelectedGame.HomeTeamFlag;
            pictureBoxAt.Image = DataSender.SelectedGame.AwayTeamFlag;
            lblScore.Text = $"{DataSender.SelectedGame.HomeTeamGoals} - {DataSender.SelectedGame.AwayTeamGoals}";

            Game game = DataSender.SelectedGame;
            int elem;
            //PictureBox[] boxes; Label[] labels;
            var keeper = game.HomeTeamKeeper; var field = game.HomeTeamPlayers;

            for (int n = 0; n < 2; n++)
            {
                elem = 0;
                if (game.GetGoalsScorers(keeper, field) != null)
                {
                    PictureBox picture = new PictureBox();
                    picture.Location = new Point(15 + 275 * n, 150 + elem * 45);
                    picture.Size = new Size(30, 30);
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/goal.png"));
                    Label label = new Label(); label.ForeColor = Color.WhiteSmoke;
                    label.Location = new Point(50 + 275 * n, 150 + elem * 45); label.Size = new Size(195, 40);
                    string[] players = game.GetGoalsScorers(keeper, field);
                    for (int i = 0; i < players.Length; i++)
                        label.Text += players[i];
                    this.Controls.Add(label); this.Controls.Add(picture);
                    elem++;
                }
                if (game.GetAssistsProviders(keeper, field) != null)
                {
                    PictureBox picture = new PictureBox();
                    picture.Location = new Point(15 + 275 * n, 150 + elem * 45);
                    picture.Size = new Size(30, 30);
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/assist.jpg"));
                    Label label = new Label(); label.ForeColor = Color.WhiteSmoke;
                    label.Location = new Point(50 + 275 * n, 150 + elem * 45); label.Size = new Size(195, 40);
                    string[] players = game.GetAssistsProviders(keeper, field);
                    label.Text = $" ";
                    for (int i = 0; i < players.Length; i++)
                        label.Text += players[i];
                    this.Controls.Add(label); this.Controls.Add(picture);
                    elem++;
                }
                if (game.GetPenaltyScorers(keeper, field) != null)
                {
                    PictureBox picture = new PictureBox();
                    picture.Location = new Point(15 + 275 * n, 150 + elem * 45);
                    picture.Size = new Size(30, 30);
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/penalty.jpg"));
                    Label label = new Label(); label.ForeColor = Color.WhiteSmoke;
                    label.Location = new Point(50 + 275 * n, 150 + elem * 45); label.Size = new Size(195, 40);
                    string[] players = game.GetPenaltyScorers(keeper, field);
                    label.Text = $"";
                    for (int i = 0; i < players.Length; i++)
                        label.Text += players[i];
                    this.Controls.Add(label); this.Controls.Add(picture);
                    elem++;
                }
                if (game.GetYellowCardsReceivers(keeper, field) != null)
                {
                    PictureBox picture = new PictureBox();
                    picture.Location = new Point(15 + 275 * n, 150 + elem * 45);
                    picture.Size = new Size(30, 30); 
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/yellowCard.png"));
                    Label label = new Label(); label.ForeColor = Color.WhiteSmoke;
                    label.Location = new Point(50 + 275 * n, 150 + elem * 45); label.Size = new Size(195, 40);
                    string[] players = game.GetYellowCardsReceivers(keeper, field);
                    label.Text = $"";
                    for (int i = 0; i < players.Length; i++)
                        label.Text += players[i];
                    this.Controls.Add(label); this.Controls.Add(picture);
                    elem++;
                }
                if (game.GetRedCardsReceivers(keeper, field) != null)
                {
                    PictureBox picture = new PictureBox();
                    picture.Location = new Point(15 + 275 * n, 150 + elem * 45);
                    picture.Size = new Size(30, 30);
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/redCard.jpg"));
                    Label label = new Label(); label.ForeColor = Color.WhiteSmoke;
                    label.Location = new Point(50 + 275 * n, 150 + elem * 45); label.Size = new Size(195, 40);
                    string[] players = game.GetRedCardsReceivers(keeper, field);
                    label.Text = $"";
                    for (int i = 0; i < players.Length; i++)
                        label.Text += players[i];
                    this.Controls.Add(label); this.Controls.Add(picture);
                    elem++;
                }
                
                if (n==0) { keeper = game.AwayTeamKeeper; field = game.AwayTeamPlayers; }
                else if(n==1){ keeper = game.HomeTeamKeeper; field = game.HomeTeamPlayers; }
                if (game.GetOwnGoalsScorers(keeper, field) != null)
                {
                    PictureBox picture = new PictureBox();
                    picture.Location = new Point(15 + 275 * n, 150 + elem * 45);
                    picture.Size = new Size(30, 30);
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Image = new Bitmap(Path.Combine(Application.StartupPath, @"images/ownGoal.jpg"));
                    Label label = new Label(); label.ForeColor = Color.WhiteSmoke;
                    label.Location = new Point(50 + 275 * n, 150 + elem * 45); label.Size = new Size(195, 40);
                    string[] players = game.GetOwnGoalsScorers(keeper, field);
                    label.Text = $"";
                    for (int i = 0; i < players.Length; i++)
                        label.Text += players[i];
                    this.Controls.Add(label); this.Controls.Add(picture);
                    elem++;
                }
            }
        }

        private void GameForm_Load(object sender, EventArgs e)
        {
            }
    }
}
