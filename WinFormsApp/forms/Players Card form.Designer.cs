﻿namespace WinFormsApp
{
    partial class PlayersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxPlayer = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.pictureBoxGoal = new System.Windows.Forms.PictureBox();
            this.pictureBoxPenalty = new System.Windows.Forms.PictureBox();
            this.pictureBoxYellow = new System.Windows.Forms.PictureBox();
            this.pictureBoxAssist = new System.Windows.Forms.PictureBox();
            this.pictureBoxOwnGoal = new System.Windows.Forms.PictureBox();
            this.pictureBoxRed = new System.Windows.Forms.PictureBox();
            this.lblGoal = new System.Windows.Forms.Label();
            this.lblRed = new System.Windows.Forms.Label();
            this.lblYellow = new System.Windows.Forms.Label();
            this.lblOwnGoal = new System.Windows.Forms.Label();
            this.lblPenalty = new System.Windows.Forms.Label();
            this.lblAssist = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGoal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPenalty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxYellow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAssist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOwnGoal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRed)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxPlayer
            // 
            this.pictureBoxPlayer.Location = new System.Drawing.Point(14, 14);
            this.pictureBoxPlayer.Name = "pictureBoxPlayer";
            this.pictureBoxPlayer.Size = new System.Drawing.Size(83, 109);
            this.pictureBoxPlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPlayer.TabIndex = 0;
            this.pictureBoxPlayer.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblAge);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.pictureBoxPlayer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 160);
            this.panel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Sitka Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(216, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "fieldplayer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Sitka Text", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(124, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Position:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Sitka Text", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(124, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Age:";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Font = new System.Drawing.Font("Sitka Text", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAge.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAge.Location = new System.Drawing.Point(175, 100);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(56, 24);
            this.lblAge.TabIndex = 2;
            this.lblAge.Text = "label1";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblName.Location = new System.Drawing.Point(124, 14);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(72, 28);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "label1";
            // 
            // pictureBoxGoal
            // 
            this.pictureBoxGoal.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxGoal.Location = new System.Drawing.Point(15, 182);
            this.pictureBoxGoal.Name = "pictureBoxGoal";
            this.pictureBoxGoal.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxGoal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGoal.TabIndex = 2;
            this.pictureBoxGoal.TabStop = false;
            // 
            // pictureBoxPenalty
            // 
            this.pictureBoxPenalty.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxPenalty.Location = new System.Drawing.Point(15, 246);
            this.pictureBoxPenalty.Name = "pictureBoxPenalty";
            this.pictureBoxPenalty.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxPenalty.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPenalty.TabIndex = 3;
            this.pictureBoxPenalty.TabStop = false;
            // 
            // pictureBoxYellow
            // 
            this.pictureBoxYellow.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxYellow.Location = new System.Drawing.Point(15, 310);
            this.pictureBoxYellow.Name = "pictureBoxYellow";
            this.pictureBoxYellow.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxYellow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxYellow.TabIndex = 4;
            this.pictureBoxYellow.TabStop = false;
            // 
            // pictureBoxAssist
            // 
            this.pictureBoxAssist.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxAssist.Location = new System.Drawing.Point(179, 182);
            this.pictureBoxAssist.Name = "pictureBoxAssist";
            this.pictureBoxAssist.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxAssist.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAssist.TabIndex = 5;
            this.pictureBoxAssist.TabStop = false;
            // 
            // pictureBoxOwnGoal
            // 
            this.pictureBoxOwnGoal.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxOwnGoal.Location = new System.Drawing.Point(179, 246);
            this.pictureBoxOwnGoal.Name = "pictureBoxOwnGoal";
            this.pictureBoxOwnGoal.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxOwnGoal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOwnGoal.TabIndex = 6;
            this.pictureBoxOwnGoal.TabStop = false;
            // 
            // pictureBoxRed
            // 
            this.pictureBoxRed.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxRed.Location = new System.Drawing.Point(179, 310);
            this.pictureBoxRed.Name = "pictureBoxRed";
            this.pictureBoxRed.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRed.TabIndex = 7;
            this.pictureBoxRed.TabStop = false;
            // 
            // lblGoal
            // 
            this.lblGoal.AutoSize = true;
            this.lblGoal.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblGoal.ForeColor = System.Drawing.Color.Moccasin;
            this.lblGoal.Location = new System.Drawing.Point(73, 195);
            this.lblGoal.Name = "lblGoal";
            this.lblGoal.Size = new System.Drawing.Size(90, 22);
            this.lblGoal.TabIndex = 8;
            this.lblGoal.Text = "lblGoals";
            // 
            // lblRed
            // 
            this.lblRed.AutoSize = true;
            this.lblRed.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblRed.ForeColor = System.Drawing.Color.Moccasin;
            this.lblRed.Location = new System.Drawing.Point(243, 321);
            this.lblRed.Name = "lblRed";
            this.lblRed.Size = new System.Drawing.Size(70, 22);
            this.lblRed.TabIndex = 9;
            this.lblRed.Text = "label4";
            // 
            // lblYellow
            // 
            this.lblYellow.AutoSize = true;
            this.lblYellow.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblYellow.ForeColor = System.Drawing.Color.Moccasin;
            this.lblYellow.Location = new System.Drawing.Point(73, 321);
            this.lblYellow.Name = "lblYellow";
            this.lblYellow.Size = new System.Drawing.Size(70, 22);
            this.lblYellow.TabIndex = 10;
            this.lblYellow.Text = "label5";
            // 
            // lblOwnGoal
            // 
            this.lblOwnGoal.AutoSize = true;
            this.lblOwnGoal.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblOwnGoal.ForeColor = System.Drawing.Color.Moccasin;
            this.lblOwnGoal.Location = new System.Drawing.Point(243, 258);
            this.lblOwnGoal.Name = "lblOwnGoal";
            this.lblOwnGoal.Size = new System.Drawing.Size(70, 22);
            this.lblOwnGoal.TabIndex = 11;
            this.lblOwnGoal.Text = "label6";
            // 
            // lblPenalty
            // 
            this.lblPenalty.AutoSize = true;
            this.lblPenalty.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblPenalty.ForeColor = System.Drawing.Color.Moccasin;
            this.lblPenalty.Location = new System.Drawing.Point(73, 258);
            this.lblPenalty.Name = "lblPenalty";
            this.lblPenalty.Size = new System.Drawing.Size(70, 22);
            this.lblPenalty.TabIndex = 12;
            this.lblPenalty.Text = "label7";
            // 
            // lblAssist
            // 
            this.lblAssist.AutoSize = true;
            this.lblAssist.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblAssist.ForeColor = System.Drawing.Color.Moccasin;
            this.lblAssist.Location = new System.Drawing.Point(243, 195);
            this.lblAssist.Name = "lblAssist";
            this.lblAssist.Size = new System.Drawing.Size(70, 22);
            this.lblAssist.TabIndex = 13;
            this.lblAssist.Text = "label8";
            // 
            // PlayersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 426);
            this.Controls.Add(this.lblAssist);
            this.Controls.Add(this.lblPenalty);
            this.Controls.Add(this.lblOwnGoal);
            this.Controls.Add(this.lblYellow);
            this.Controls.Add(this.lblRed);
            this.Controls.Add(this.lblGoal);
            this.Controls.Add(this.pictureBoxRed);
            this.Controls.Add(this.pictureBoxOwnGoal);
            this.Controls.Add(this.pictureBoxAssist);
            this.Controls.Add(this.pictureBoxYellow);
            this.Controls.Add(this.pictureBoxPenalty);
            this.Controls.Add(this.pictureBoxGoal);
            this.Controls.Add(this.panel1);
            this.Name = "PlayersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Field player card";
            this.Load += new System.EventHandler(this.PlayersForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGoal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPenalty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxYellow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAssist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOwnGoal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PictureBox pictureBoxPlayer;
        private Panel panel1;
        private Label lblAge;
        private Label lblName;
        private Label label1;
        private Label label3;
        private Label label2;
        private PictureBox pictureBoxGoal;
        private PictureBox pictureBoxPenalty;
        private PictureBox pictureBoxYellow;
        private PictureBox pictureBoxAssist;
        private PictureBox pictureBoxOwnGoal;
        private PictureBox pictureBoxRed;
        private Label lblGoal;
        private Label lblRed;
        private Label lblYellow;
        private Label lblOwnGoal;
        private Label lblPenalty;
        private Label lblAssist;
    }
}