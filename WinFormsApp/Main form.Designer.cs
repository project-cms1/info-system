﻿namespace WinFormsApp
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.MainTable = new System.Windows.Forms.TabPage();
            this.dgvMainTable = new System.Windows.Forms.DataGridView();
            this.PlayersStats = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPlayersFilter = new System.Windows.Forms.TextBox();
            this.lblSortBy = new System.Windows.Forms.Label();
            this.comBoxPlayersSort = new System.Windows.Forms.ComboBox();
            this.comBoxPlayersTeams = new System.Windows.Forms.ComboBox();
            this.dgvPlayers = new System.Windows.Forms.DataGridView();
            this.KeepersStats = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxKeepersFilter = new System.Windows.Forms.TextBox();
            this.comBoxKeepersSort = new System.Windows.Forms.ComboBox();
            this.comBoxKeepersTeams = new System.Windows.Forms.ComboBox();
            this.dgvKeepers = new System.Windows.Forms.DataGridView();
            this.GamesResults = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvResults = new System.Windows.Forms.DataGridView();
            this.comBoxResults = new System.Windows.Forms.ComboBox();
            this.AddGame = new System.Windows.Forms.TabPage();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblKeeper = new System.Windows.Forms.Label();
            this.comBoxAtKeeper = new System.Windows.Forms.ComboBox();
            this.comBoxHtKeeper = new System.Windows.Forms.ComboBox();
            this.lblGoals = new System.Windows.Forms.Label();
            this.lblTeams = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.buttonSaveGame = new System.Windows.Forms.Button();
            this.groupBoxAction = new System.Windows.Forms.GroupBox();
            this.lblScorer = new System.Windows.Forms.Label();
            this.lblAssistant = new System.Windows.Forms.Label();
            this.radioBtnAt = new System.Windows.Forms.RadioButton();
            this.radioBtnHt = new System.Windows.Forms.RadioButton();
            this.checkBoxOwnGoal = new System.Windows.Forms.CheckBox();
            this.btnSaveAction = new System.Windows.Forms.Button();
            this.checkBoxPenOrRed = new System.Windows.Forms.CheckBox();
            this.comboBoxPlayersAdditional = new System.Windows.Forms.ComboBox();
            this.comboBoxPlayers = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.numAtGoals = new System.Windows.Forms.NumericUpDown();
            this.numHtGoals = new System.Windows.Forms.NumericUpDown();
            this.buttonSaveResult = new System.Windows.Forms.Button();
            this.comBoxAt = new System.Windows.Forms.ComboBox();
            this.comBoxHt = new System.Windows.Forms.ComboBox();
            this.groupBoxCards = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAtCardsY = new System.Windows.Forms.Label();
            this.pictureBoxRed = new System.Windows.Forms.PictureBox();
            this.pictureBoxYellow = new System.Windows.Forms.PictureBox();
            this.lblHtCardsR = new System.Windows.Forms.Label();
            this.lblAtCardsR = new System.Windows.Forms.Label();
            this.lblHtCardsY = new System.Windows.Forms.Label();
            this.groupBoxScore = new System.Windows.Forms.GroupBox();
            this.lblScoreText = new System.Windows.Forms.Label();
            this.lblScore = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.MainTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainTable)).BeginInit();
            this.PlayersStats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlayers)).BeginInit();
            this.KeepersStats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKeepers)).BeginInit();
            this.GamesResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).BeginInit();
            this.AddGame.SuspendLayout();
            this.groupBoxAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAtGoals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHtGoals)).BeginInit();
            this.groupBoxCards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxYellow)).BeginInit();
            this.groupBoxScore.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl.Controls.Add(this.MainTable);
            this.tabControl.Controls.Add(this.PlayersStats);
            this.tabControl.Controls.Add(this.KeepersStats);
            this.tabControl.Controls.Add(this.GamesResults);
            this.tabControl.Controls.Add(this.AddGame);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tabControl.ItemSize = new System.Drawing.Size(70, 30);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(859, 626);
            this.tabControl.TabIndex = 0;
            // 
            // MainTable
            // 
            this.MainTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(20)))), ((int)(((byte)(29)))));
            this.MainTable.Controls.Add(this.dgvMainTable);
            this.MainTable.Location = new System.Drawing.Point(4, 34);
            this.MainTable.Name = "MainTable";
            this.MainTable.Padding = new System.Windows.Forms.Padding(3);
            this.MainTable.Size = new System.Drawing.Size(851, 588);
            this.MainTable.TabIndex = 0;
            this.MainTable.Text = "Main table";
            // 
            // dgvMainTable
            // 
            this.dgvMainTable.AllowUserToAddRows = false;
            this.dgvMainTable.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(35)))), ((int)(((byte)(45)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Trebuchet MS", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMainTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMainTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(50)))), ((int)(((byte)(60)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMainTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMainTable.EnableHeadersVisualStyles = false;
            this.dgvMainTable.GridColor = System.Drawing.Color.LightBlue;
            this.dgvMainTable.Location = new System.Drawing.Point(22, 45);
            this.dgvMainTable.Name = "dgvMainTable";
            this.dgvMainTable.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMainTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMainTable.RowHeadersVisible = false;
            this.dgvMainTable.RowTemplate.Height = 25;
            this.dgvMainTable.Size = new System.Drawing.Size(805, 255);
            this.dgvMainTable.TabIndex = 2;
            this.dgvMainTable.UseWaitCursor = true;
            // 
            // PlayersStats
            // 
            this.PlayersStats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(20)))), ((int)(((byte)(29)))));
            this.PlayersStats.Controls.Add(this.label3);
            this.PlayersStats.Controls.Add(this.textBoxPlayersFilter);
            this.PlayersStats.Controls.Add(this.lblSortBy);
            this.PlayersStats.Controls.Add(this.comBoxPlayersSort);
            this.PlayersStats.Controls.Add(this.comBoxPlayersTeams);
            this.PlayersStats.Controls.Add(this.dgvPlayers);
            this.PlayersStats.Location = new System.Drawing.Point(4, 34);
            this.PlayersStats.Name = "PlayersStats";
            this.PlayersStats.Padding = new System.Windows.Forms.Padding(3);
            this.PlayersStats.Size = new System.Drawing.Size(851, 588);
            this.PlayersStats.TabIndex = 1;
            this.PlayersStats.Text = "PLayers stats";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(579, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 30);
            this.label3.TabIndex = 13;
            this.label3.Text = "Filter by:";
            this.label3.UseWaitCursor = true;
            // 
            // textBoxPlayersFilter
            // 
            this.textBoxPlayersFilter.Location = new System.Drawing.Point(689, 68);
            this.textBoxPlayersFilter.Name = "textBoxPlayersFilter";
            this.textBoxPlayersFilter.PlaceholderText = "Surname...";
            this.textBoxPlayersFilter.Size = new System.Drawing.Size(119, 23);
            this.textBoxPlayersFilter.TabIndex = 12;
            this.textBoxPlayersFilter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPlayersFilter.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblSortBy
            // 
            this.lblSortBy.AutoSize = true;
            this.lblSortBy.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblSortBy.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblSortBy.Location = new System.Drawing.Point(304, 24);
            this.lblSortBy.Name = "lblSortBy";
            this.lblSortBy.Size = new System.Drawing.Size(101, 30);
            this.lblSortBy.TabIndex = 11;
            this.lblSortBy.Text = "Sort by:";
            this.lblSortBy.UseWaitCursor = true;
            // 
            // comBoxPlayersSort
            // 
            this.comBoxPlayersSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxPlayersSort.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxPlayersSort.FormattingEnabled = true;
            this.comBoxPlayersSort.Items.AddRange(new object[] {
            "None",
            "Goals scored",
            "Assists",
            "Yellow cards",
            "Red cards",
            "Own goals",
            "Penalty"});
            this.comBoxPlayersSort.Location = new System.Drawing.Point(429, 67);
            this.comBoxPlayersSort.Name = "comBoxPlayersSort";
            this.comBoxPlayersSort.Size = new System.Drawing.Size(121, 26);
            this.comBoxPlayersSort.TabIndex = 10;
            this.comBoxPlayersSort.UseWaitCursor = true;
            this.comBoxPlayersSort.SelectedIndexChanged += new System.EventHandler(this.comBoxPlayersSort_SelectedIndexChanged);
            // 
            // comBoxPlayersTeams
            // 
            this.comBoxPlayersTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxPlayersTeams.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxPlayersTeams.FormattingEnabled = true;
            this.comBoxPlayersTeams.Location = new System.Drawing.Point(167, 67);
            this.comBoxPlayersTeams.Name = "comBoxPlayersTeams";
            this.comBoxPlayersTeams.Size = new System.Drawing.Size(121, 26);
            this.comBoxPlayersTeams.TabIndex = 9;
            this.comBoxPlayersTeams.UseWaitCursor = true;
            this.comBoxPlayersTeams.SelectedIndexChanged += new System.EventHandler(this.comBoxPlayersTeams_SelectedIndexChanged);
            // 
            // dgvPlayers
            // 
            this.dgvPlayers.AllowUserToAddRows = false;
            this.dgvPlayers.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(35)))), ((int)(((byte)(45)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Trebuchet MS", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPlayers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPlayers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(50)))), ((int)(((byte)(60)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPlayers.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPlayers.EnableHeadersVisualStyles = false;
            this.dgvPlayers.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvPlayers.Location = new System.Drawing.Point(23, 128);
            this.dgvPlayers.Name = "dgvPlayers";
            this.dgvPlayers.ReadOnly = true;
            this.dgvPlayers.RowHeadersVisible = false;
            this.dgvPlayers.RowTemplate.Height = 25;
            this.dgvPlayers.Size = new System.Drawing.Size(820, 355);
            this.dgvPlayers.TabIndex = 8;
            this.dgvPlayers.UseWaitCursor = true;
            this.dgvPlayers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPlayers_CellDoubleClick);
            // 
            // KeepersStats
            // 
            this.KeepersStats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(20)))), ((int)(((byte)(29)))));
            this.KeepersStats.Controls.Add(this.label5);
            this.KeepersStats.Controls.Add(this.label6);
            this.KeepersStats.Controls.Add(this.textBoxKeepersFilter);
            this.KeepersStats.Controls.Add(this.comBoxKeepersSort);
            this.KeepersStats.Controls.Add(this.comBoxKeepersTeams);
            this.KeepersStats.Controls.Add(this.dgvKeepers);
            this.KeepersStats.Location = new System.Drawing.Point(4, 34);
            this.KeepersStats.Name = "KeepersStats";
            this.KeepersStats.Size = new System.Drawing.Size(851, 588);
            this.KeepersStats.TabIndex = 2;
            this.KeepersStats.Text = "Keepers stats";
            this.KeepersStats.Click += new System.EventHandler(this.KeepersStats_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(557, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 30);
            this.label5.TabIndex = 15;
            this.label5.Text = "Filter by:";
            this.label5.UseWaitCursor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(268, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 30);
            this.label6.TabIndex = 14;
            this.label6.Text = "Sort by:";
            this.label6.UseWaitCursor = true;
            // 
            // textBoxKeepersFilter
            // 
            this.textBoxKeepersFilter.Location = new System.Drawing.Point(669, 70);
            this.textBoxKeepersFilter.Name = "textBoxKeepersFilter";
            this.textBoxKeepersFilter.PlaceholderText = "Surname...";
            this.textBoxKeepersFilter.Size = new System.Drawing.Size(119, 23);
            this.textBoxKeepersFilter.TabIndex = 13;
            this.textBoxKeepersFilter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxKeepersFilter.TextChanged += new System.EventHandler(this.textBoxKeepersFilter_TextChanged);
            // 
            // comBoxKeepersSort
            // 
            this.comBoxKeepersSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxKeepersSort.FormattingEnabled = true;
            this.comBoxKeepersSort.Items.AddRange(new object[] {
            "None",
            "Clean sheets",
            "Goals conceded",
            "Goals ",
            "Assists",
            "Yellow cards",
            "Red cards",
            "Own goals",
            "Penalty goals"});
            this.comBoxKeepersSort.Location = new System.Drawing.Point(377, 67);
            this.comBoxKeepersSort.Name = "comBoxKeepersSort";
            this.comBoxKeepersSort.Size = new System.Drawing.Size(121, 26);
            this.comBoxKeepersSort.TabIndex = 11;
            this.comBoxKeepersSort.UseWaitCursor = true;
            this.comBoxKeepersSort.SelectedIndexChanged += new System.EventHandler(this.comBoxKeepersSort_SelectedIndexChanged);
            // 
            // comBoxKeepersTeams
            // 
            this.comBoxKeepersTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxKeepersTeams.FormattingEnabled = true;
            this.comBoxKeepersTeams.Location = new System.Drawing.Point(163, 70);
            this.comBoxKeepersTeams.Name = "comBoxKeepersTeams";
            this.comBoxKeepersTeams.Size = new System.Drawing.Size(121, 26);
            this.comBoxKeepersTeams.TabIndex = 10;
            this.comBoxKeepersTeams.UseWaitCursor = true;
            this.comBoxKeepersTeams.SelectedIndexChanged += new System.EventHandler(this.comBoxKeepersTeams_SelectedIndexChanged);
            // 
            // dgvKeepers
            // 
            this.dgvKeepers.AllowUserToAddRows = false;
            this.dgvKeepers.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(35)))), ((int)(((byte)(45)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Trebuchet MS", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKeepers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvKeepers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(50)))), ((int)(((byte)(60)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKeepers.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvKeepers.EnableHeadersVisualStyles = false;
            this.dgvKeepers.Location = new System.Drawing.Point(3, 132);
            this.dgvKeepers.Name = "dgvKeepers";
            this.dgvKeepers.ReadOnly = true;
            this.dgvKeepers.RowHeadersVisible = false;
            this.dgvKeepers.RowTemplate.Height = 25;
            this.dgvKeepers.Size = new System.Drawing.Size(848, 189);
            this.dgvKeepers.TabIndex = 9;
            this.dgvKeepers.UseWaitCursor = true;
            this.dgvKeepers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKeepers_CellDoubleClick);
            // 
            // GamesResults
            // 
            this.GamesResults.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(20)))), ((int)(((byte)(29)))));
            this.GamesResults.Controls.Add(this.label4);
            this.GamesResults.Controls.Add(this.dgvResults);
            this.GamesResults.Controls.Add(this.comBoxResults);
            this.GamesResults.Location = new System.Drawing.Point(4, 34);
            this.GamesResults.Name = "GamesResults";
            this.GamesResults.Size = new System.Drawing.Size(851, 588);
            this.GamesResults.TabIndex = 3;
            this.GamesResults.Text = "Games results";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(374, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 30);
            this.label4.TabIndex = 15;
            this.label4.Text = "Sort by:";
            this.label4.UseWaitCursor = true;
            // 
            // dgvResults
            // 
            this.dgvResults.AllowUserToAddRows = false;
            this.dgvResults.AllowUserToDeleteRows = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(35)))), ((int)(((byte)(45)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Trebuchet MS", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(50)))), ((int)(((byte)(60)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvResults.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvResults.EnableHeadersVisualStyles = false;
            this.dgvResults.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvResults.Location = new System.Drawing.Point(79, 158);
            this.dgvResults.Name = "dgvResults";
            this.dgvResults.ReadOnly = true;
            this.dgvResults.RowHeadersVisible = false;
            this.dgvResults.RowTemplate.Height = 25;
            this.dgvResults.Size = new System.Drawing.Size(694, 156);
            this.dgvResults.TabIndex = 5;
            this.dgvResults.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResults_CellDoubleClick);
            // 
            // comBoxResults
            // 
            this.comBoxResults.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxResults.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxResults.FormattingEnabled = true;
            this.comBoxResults.Location = new System.Drawing.Point(374, 88);
            this.comBoxResults.Name = "comBoxResults";
            this.comBoxResults.Size = new System.Drawing.Size(121, 30);
            this.comBoxResults.TabIndex = 4;
            this.comBoxResults.UseWaitCursor = true;
            this.comBoxResults.SelectedIndexChanged += new System.EventHandler(this.comboBoxResults_SelectedIndexChanged);
            // 
            // AddGame
            // 
            this.AddGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(20)))), ((int)(((byte)(29)))));
            this.AddGame.Controls.Add(this.lblDate);
            this.AddGame.Controls.Add(this.lblKeeper);
            this.AddGame.Controls.Add(this.comBoxAtKeeper);
            this.AddGame.Controls.Add(this.comBoxHtKeeper);
            this.AddGame.Controls.Add(this.lblGoals);
            this.AddGame.Controls.Add(this.lblTeams);
            this.AddGame.Controls.Add(this.lblInfo);
            this.AddGame.Controls.Add(this.buttonSaveGame);
            this.AddGame.Controls.Add(this.groupBoxAction);
            this.AddGame.Controls.Add(this.label2);
            this.AddGame.Controls.Add(this.textBoxDate);
            this.AddGame.Controls.Add(this.numAtGoals);
            this.AddGame.Controls.Add(this.numHtGoals);
            this.AddGame.Controls.Add(this.buttonSaveResult);
            this.AddGame.Controls.Add(this.comBoxAt);
            this.AddGame.Controls.Add(this.comBoxHt);
            this.AddGame.Controls.Add(this.groupBoxCards);
            this.AddGame.Controls.Add(this.groupBoxScore);
            this.AddGame.Location = new System.Drawing.Point(4, 34);
            this.AddGame.Name = "AddGame";
            this.AddGame.Size = new System.Drawing.Size(851, 588);
            this.AddGame.TabIndex = 4;
            this.AddGame.Text = "Add game";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblDate.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblDate.Location = new System.Drawing.Point(682, 80);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(62, 28);
            this.lblDate.TabIndex = 47;
            this.lblDate.Text = "Date:";
            this.lblDate.UseWaitCursor = true;
            // 
            // lblKeeper
            // 
            this.lblKeeper.AutoSize = true;
            this.lblKeeper.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblKeeper.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblKeeper.Location = new System.Drawing.Point(368, 118);
            this.lblKeeper.Name = "lblKeeper";
            this.lblKeeper.Size = new System.Drawing.Size(124, 28);
            this.lblKeeper.TabIndex = 45;
            this.lblKeeper.Text = "Goalkeeper:";
            this.lblKeeper.UseWaitCursor = true;
            // 
            // comBoxAtKeeper
            // 
            this.comBoxAtKeeper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxAtKeeper.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxAtKeeper.FormattingEnabled = true;
            this.comBoxAtKeeper.Location = new System.Drawing.Point(508, 131);
            this.comBoxAtKeeper.Name = "comBoxAtKeeper";
            this.comBoxAtKeeper.Size = new System.Drawing.Size(133, 28);
            this.comBoxAtKeeper.TabIndex = 44;
            this.comBoxAtKeeper.UseWaitCursor = true;
            // 
            // comBoxHtKeeper
            // 
            this.comBoxHtKeeper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxHtKeeper.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxHtKeeper.FormattingEnabled = true;
            this.comBoxHtKeeper.Location = new System.Drawing.Point(208, 131);
            this.comBoxHtKeeper.Name = "comBoxHtKeeper";
            this.comBoxHtKeeper.Size = new System.Drawing.Size(126, 28);
            this.comBoxHtKeeper.TabIndex = 43;
            this.comBoxHtKeeper.UseWaitCursor = true;
            // 
            // lblGoals
            // 
            this.lblGoals.AutoSize = true;
            this.lblGoals.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblGoals.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblGoals.Location = new System.Drawing.Point(389, 68);
            this.lblGoals.Name = "lblGoals";
            this.lblGoals.Size = new System.Drawing.Size(72, 28);
            this.lblGoals.TabIndex = 42;
            this.lblGoals.Text = "Goals:";
            this.lblGoals.UseWaitCursor = true;
            // 
            // lblTeams
            // 
            this.lblTeams.AutoSize = true;
            this.lblTeams.Font = new System.Drawing.Font("Sitka Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTeams.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTeams.Location = new System.Drawing.Point(389, 16);
            this.lblTeams.Name = "lblTeams";
            this.lblTeams.Size = new System.Drawing.Size(80, 28);
            this.lblTeams.TabIndex = 41;
            this.lblTeams.Text = "Teams:";
            this.lblTeams.UseWaitCursor = true;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblInfo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblInfo.Location = new System.Drawing.Point(377, 251);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(58, 21);
            this.lblInfo.TabIndex = 39;
            this.lblInfo.Text = "label4";
            this.lblInfo.UseWaitCursor = true;
            this.lblInfo.Visible = false;
            // 
            // buttonSaveGame
            // 
            this.buttonSaveGame.BackColor = System.Drawing.Color.ForestGreen;
            this.buttonSaveGame.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(255)))), ((int)(((byte)(200)))));
            this.buttonSaveGame.FlatAppearance.BorderSize = 3;
            this.buttonSaveGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveGame.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonSaveGame.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.buttonSaveGame.Location = new System.Drawing.Point(352, 540);
            this.buttonSaveGame.Name = "buttonSaveGame";
            this.buttonSaveGame.Size = new System.Drawing.Size(164, 48);
            this.buttonSaveGame.TabIndex = 38;
            this.buttonSaveGame.Text = "Save game";
            this.buttonSaveGame.UseVisualStyleBackColor = false;
            this.buttonSaveGame.UseWaitCursor = true;
            this.buttonSaveGame.Visible = false;
            this.buttonSaveGame.Click += new System.EventHandler(this.buttonSaveGame_Click);
            // 
            // groupBoxAction
            // 
            this.groupBoxAction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.groupBoxAction.Controls.Add(this.lblScorer);
            this.groupBoxAction.Controls.Add(this.lblAssistant);
            this.groupBoxAction.Controls.Add(this.radioBtnAt);
            this.groupBoxAction.Controls.Add(this.radioBtnHt);
            this.groupBoxAction.Controls.Add(this.checkBoxOwnGoal);
            this.groupBoxAction.Controls.Add(this.btnSaveAction);
            this.groupBoxAction.Controls.Add(this.checkBoxPenOrRed);
            this.groupBoxAction.Controls.Add(this.comboBoxPlayersAdditional);
            this.groupBoxAction.Controls.Add(this.comboBoxPlayers);
            this.groupBoxAction.Location = new System.Drawing.Point(228, 272);
            this.groupBoxAction.Name = "groupBoxAction";
            this.groupBoxAction.Size = new System.Drawing.Size(422, 265);
            this.groupBoxAction.TabIndex = 37;
            this.groupBoxAction.TabStop = false;
            this.groupBoxAction.UseWaitCursor = true;
            this.groupBoxAction.Visible = false;
            // 
            // lblScorer
            // 
            this.lblScorer.AutoSize = true;
            this.lblScorer.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lblScorer.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblScorer.Location = new System.Drawing.Point(28, 63);
            this.lblScorer.Name = "lblScorer";
            this.lblScorer.Size = new System.Drawing.Size(99, 22);
            this.lblScorer.TabIndex = 17;
            this.lblScorer.Text = "Goal scorer:";
            this.lblScorer.UseWaitCursor = true;
            // 
            // lblAssistant
            // 
            this.lblAssistant.AutoSize = true;
            this.lblAssistant.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lblAssistant.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAssistant.Location = new System.Drawing.Point(28, 146);
            this.lblAssistant.Name = "lblAssistant";
            this.lblAssistant.Size = new System.Drawing.Size(124, 22);
            this.lblAssistant.TabIndex = 16;
            this.lblAssistant.Text = "Assist provider:";
            this.lblAssistant.UseWaitCursor = true;
            // 
            // radioBtnAt
            // 
            this.radioBtnAt.AutoSize = true;
            this.radioBtnAt.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.radioBtnAt.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.radioBtnAt.Location = new System.Drawing.Point(151, 24);
            this.radioBtnAt.Name = "radioBtnAt";
            this.radioBtnAt.Size = new System.Drawing.Size(124, 22);
            this.radioBtnAt.TabIndex = 15;
            this.radioBtnAt.TabStop = true;
            this.radioBtnAt.Text = "radioButton2";
            this.radioBtnAt.UseVisualStyleBackColor = true;
            this.radioBtnAt.UseWaitCursor = true;
            // 
            // radioBtnHt
            // 
            this.radioBtnHt.AutoSize = true;
            this.radioBtnHt.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.radioBtnHt.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.radioBtnHt.Location = new System.Drawing.Point(28, 24);
            this.radioBtnHt.Name = "radioBtnHt";
            this.radioBtnHt.Size = new System.Drawing.Size(124, 22);
            this.radioBtnHt.TabIndex = 14;
            this.radioBtnHt.TabStop = true;
            this.radioBtnHt.Text = "radioButton1";
            this.radioBtnHt.UseVisualStyleBackColor = true;
            this.radioBtnHt.UseWaitCursor = true;
            this.radioBtnHt.CheckedChanged += new System.EventHandler(this.radioBtnHt_CheckedChanged);
            // 
            // checkBoxOwnGoal
            // 
            this.checkBoxOwnGoal.AutoSize = true;
            this.checkBoxOwnGoal.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxOwnGoal.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.checkBoxOwnGoal.Location = new System.Drawing.Point(282, 100);
            this.checkBoxOwnGoal.Name = "checkBoxOwnGoal";
            this.checkBoxOwnGoal.Size = new System.Drawing.Size(84, 21);
            this.checkBoxOwnGoal.TabIndex = 13;
            this.checkBoxOwnGoal.Text = "Own goal";
            this.checkBoxOwnGoal.UseVisualStyleBackColor = true;
            this.checkBoxOwnGoal.UseWaitCursor = true;
            this.checkBoxOwnGoal.CheckedChanged += new System.EventHandler(this.checkBoxOwnGoal_CheckedChanged);
            // 
            // btnSaveAction
            // 
            this.btnSaveAction.BackColor = System.Drawing.Color.SteelBlue;
            this.btnSaveAction.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSaveAction.FlatAppearance.BorderSize = 2;
            this.btnSaveAction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAction.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSaveAction.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSaveAction.Location = new System.Drawing.Point(245, 170);
            this.btnSaveAction.Name = "btnSaveAction";
            this.btnSaveAction.Size = new System.Drawing.Size(129, 72);
            this.btnSaveAction.TabIndex = 0;
            this.btnSaveAction.Text = "Save action";
            this.btnSaveAction.UseVisualStyleBackColor = false;
            this.btnSaveAction.UseWaitCursor = true;
            this.btnSaveAction.Click += new System.EventHandler(this.btnSaveAction_Click);
            // 
            // checkBoxPenOrRed
            // 
            this.checkBoxPenOrRed.AutoSize = true;
            this.checkBoxPenOrRed.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.checkBoxPenOrRed.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.checkBoxPenOrRed.Location = new System.Drawing.Point(187, 100);
            this.checkBoxPenOrRed.Name = "checkBoxPenOrRed";
            this.checkBoxPenOrRed.Size = new System.Drawing.Size(71, 21);
            this.checkBoxPenOrRed.TabIndex = 12;
            this.checkBoxPenOrRed.Text = "Penalty";
            this.checkBoxPenOrRed.UseVisualStyleBackColor = true;
            this.checkBoxPenOrRed.UseWaitCursor = true;
            this.checkBoxPenOrRed.CheckedChanged += new System.EventHandler(this.checkBoxPenOrRed_CheckedChanged);
            // 
            // comboBoxPlayersAdditional
            // 
            this.comboBoxPlayersAdditional.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPlayersAdditional.FormattingEnabled = true;
            this.comboBoxPlayersAdditional.Location = new System.Drawing.Point(28, 187);
            this.comboBoxPlayersAdditional.Name = "comboBoxPlayersAdditional";
            this.comboBoxPlayersAdditional.Size = new System.Drawing.Size(141, 26);
            this.comboBoxPlayersAdditional.TabIndex = 10;
            this.comboBoxPlayersAdditional.UseWaitCursor = true;
            // 
            // comboBoxPlayers
            // 
            this.comboBoxPlayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPlayers.FormattingEnabled = true;
            this.comboBoxPlayers.Location = new System.Drawing.Point(28, 100);
            this.comboBoxPlayers.Name = "comboBoxPlayers";
            this.comboBoxPlayers.Size = new System.Drawing.Size(141, 26);
            this.comboBoxPlayers.TabIndex = 8;
            this.comboBoxPlayers.UseWaitCursor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(10, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 24);
            this.label2.TabIndex = 36;
            this.label2.Text = "lblRound";
            this.label2.UseWaitCursor = true;
            // 
            // textBoxDate
            // 
            this.textBoxDate.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxDate.Location = new System.Drawing.Point(682, 130);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.PlaceholderText = "Y-M-D H:M";
            this.textBoxDate.Size = new System.Drawing.Size(156, 26);
            this.textBoxDate.TabIndex = 35;
            this.textBoxDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxDate.UseWaitCursor = true;
            // 
            // numAtGoals
            // 
            this.numAtGoals.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.numAtGoals.Location = new System.Drawing.Point(508, 80);
            this.numAtGoals.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numAtGoals.Name = "numAtGoals";
            this.numAtGoals.Size = new System.Drawing.Size(94, 27);
            this.numAtGoals.TabIndex = 34;
            this.numAtGoals.UseWaitCursor = true;
            // 
            // numHtGoals
            // 
            this.numHtGoals.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.numHtGoals.Location = new System.Drawing.Point(245, 80);
            this.numHtGoals.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numHtGoals.Name = "numHtGoals";
            this.numHtGoals.Size = new System.Drawing.Size(89, 27);
            this.numHtGoals.TabIndex = 33;
            this.numHtGoals.UseWaitCursor = true;
            // 
            // buttonSaveResult
            // 
            this.buttonSaveResult.BackColor = System.Drawing.Color.Firebrick;
            this.buttonSaveResult.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.buttonSaveResult.FlatAppearance.BorderSize = 2;
            this.buttonSaveResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveResult.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonSaveResult.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.buttonSaveResult.Location = new System.Drawing.Point(336, 197);
            this.buttonSaveResult.Name = "buttonSaveResult";
            this.buttonSaveResult.Size = new System.Drawing.Size(178, 35);
            this.buttonSaveResult.TabIndex = 32;
            this.buttonSaveResult.Text = "Save result";
            this.buttonSaveResult.UseVisualStyleBackColor = false;
            this.buttonSaveResult.UseWaitCursor = true;
            this.buttonSaveResult.Click += new System.EventHandler(this.buttonSaveResult_Click);
            // 
            // comBoxAt
            // 
            this.comBoxAt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxAt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxAt.FormattingEnabled = true;
            this.comBoxAt.Location = new System.Drawing.Point(508, 25);
            this.comBoxAt.Name = "comBoxAt";
            this.comBoxAt.Size = new System.Drawing.Size(133, 28);
            this.comBoxAt.TabIndex = 31;
            this.comBoxAt.UseWaitCursor = true;
            this.comBoxAt.SelectedIndexChanged += new System.EventHandler(this.comBoxAt_SelectedIndexChanged);
            // 
            // comBoxHt
            // 
            this.comBoxHt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoxHt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comBoxHt.FormattingEnabled = true;
            this.comBoxHt.Location = new System.Drawing.Point(208, 25);
            this.comBoxHt.Name = "comBoxHt";
            this.comBoxHt.Size = new System.Drawing.Size(126, 28);
            this.comBoxHt.TabIndex = 30;
            this.comBoxHt.UseWaitCursor = true;
            this.comBoxHt.SelectedIndexChanged += new System.EventHandler(this.comBoxHt_SelectedIndexChanged);
            // 
            // groupBoxCards
            // 
            this.groupBoxCards.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.groupBoxCards.Controls.Add(this.label1);
            this.groupBoxCards.Controls.Add(this.lblAtCardsY);
            this.groupBoxCards.Controls.Add(this.pictureBoxRed);
            this.groupBoxCards.Controls.Add(this.pictureBoxYellow);
            this.groupBoxCards.Controls.Add(this.lblHtCardsR);
            this.groupBoxCards.Controls.Add(this.lblAtCardsR);
            this.groupBoxCards.Controls.Add(this.lblHtCardsY);
            this.groupBoxCards.Location = new System.Drawing.Point(670, 193);
            this.groupBoxCards.Name = "groupBoxCards";
            this.groupBoxCards.Size = new System.Drawing.Size(178, 151);
            this.groupBoxCards.TabIndex = 40;
            this.groupBoxCards.TabStop = false;
            this.groupBoxCards.UseWaitCursor = true;
            this.groupBoxCards.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Orange;
            this.label1.Location = new System.Drawing.Point(65, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 25);
            this.label1.TabIndex = 23;
            this.label1.Text = "Cards:";
            this.label1.UseWaitCursor = true;
            // 
            // lblAtCardsY
            // 
            this.lblAtCardsY.AutoSize = true;
            this.lblAtCardsY.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAtCardsY.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAtCardsY.Location = new System.Drawing.Point(127, 55);
            this.lblAtCardsY.Name = "lblAtCardsY";
            this.lblAtCardsY.Size = new System.Drawing.Size(18, 18);
            this.lblAtCardsY.TabIndex = 19;
            this.lblAtCardsY.Text = "0";
            this.lblAtCardsY.UseWaitCursor = true;
            // 
            // pictureBoxRed
            // 
            this.pictureBoxRed.Image = global::WinFormsApp.Properties.Resources.redCard1;
            this.pictureBoxRed.Location = new System.Drawing.Point(31, 100);
            this.pictureBoxRed.Name = "pictureBoxRed";
            this.pictureBoxRed.Size = new System.Drawing.Size(30, 30);
            this.pictureBoxRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRed.TabIndex = 32;
            this.pictureBoxRed.TabStop = false;
            this.pictureBoxRed.UseWaitCursor = true;
            // 
            // pictureBoxYellow
            // 
            this.pictureBoxYellow.Image = global::WinFormsApp.Properties.Resources.yellowCard1;
            this.pictureBoxYellow.Location = new System.Drawing.Point(31, 51);
            this.pictureBoxYellow.Name = "pictureBoxYellow";
            this.pictureBoxYellow.Size = new System.Drawing.Size(30, 30);
            this.pictureBoxYellow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxYellow.TabIndex = 33;
            this.pictureBoxYellow.TabStop = false;
            this.pictureBoxYellow.UseWaitCursor = true;
            // 
            // lblHtCardsR
            // 
            this.lblHtCardsR.AutoSize = true;
            this.lblHtCardsR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblHtCardsR.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblHtCardsR.Location = new System.Drawing.Point(65, 106);
            this.lblHtCardsR.Name = "lblHtCardsR";
            this.lblHtCardsR.Size = new System.Drawing.Size(18, 18);
            this.lblHtCardsR.TabIndex = 18;
            this.lblHtCardsR.Text = "0";
            this.lblHtCardsR.UseWaitCursor = true;
            // 
            // lblAtCardsR
            // 
            this.lblAtCardsR.AutoSize = true;
            this.lblAtCardsR.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAtCardsR.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAtCardsR.Location = new System.Drawing.Point(127, 106);
            this.lblAtCardsR.Name = "lblAtCardsR";
            this.lblAtCardsR.Size = new System.Drawing.Size(18, 18);
            this.lblAtCardsR.TabIndex = 20;
            this.lblAtCardsR.Text = "0";
            this.lblAtCardsR.UseWaitCursor = true;
            // 
            // lblHtCardsY
            // 
            this.lblHtCardsY.AutoSize = true;
            this.lblHtCardsY.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblHtCardsY.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblHtCardsY.Location = new System.Drawing.Point(65, 55);
            this.lblHtCardsY.Name = "lblHtCardsY";
            this.lblHtCardsY.Size = new System.Drawing.Size(18, 18);
            this.lblHtCardsY.TabIndex = 17;
            this.lblHtCardsY.Text = "0";
            this.lblHtCardsY.UseWaitCursor = true;
            // 
            // groupBoxScore
            // 
            this.groupBoxScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.groupBoxScore.Controls.Add(this.lblScoreText);
            this.groupBoxScore.Controls.Add(this.lblScore);
            this.groupBoxScore.Location = new System.Drawing.Point(44, 193);
            this.groupBoxScore.Name = "groupBoxScore";
            this.groupBoxScore.Size = new System.Drawing.Size(146, 115);
            this.groupBoxScore.TabIndex = 46;
            this.groupBoxScore.TabStop = false;
            this.groupBoxScore.UseWaitCursor = true;
            this.groupBoxScore.Visible = false;
            // 
            // lblScoreText
            // 
            this.lblScoreText.AutoSize = true;
            this.lblScoreText.Font = new System.Drawing.Font("Malgun Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblScoreText.ForeColor = System.Drawing.Color.Red;
            this.lblScoreText.Location = new System.Drawing.Point(43, 21);
            this.lblScoreText.Name = "lblScoreText";
            this.lblScoreText.Size = new System.Drawing.Size(66, 25);
            this.lblScoreText.TabIndex = 22;
            this.lblScoreText.Text = "Score:";
            this.lblScoreText.UseWaitCursor = true;
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblScore.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblScore.Location = new System.Drawing.Point(44, 64);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(80, 25);
            this.lblScore.TabIndex = 21;
            this.lblScore.Text = "label4";
            this.lblScore.UseWaitCursor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 626);
            this.Controls.Add(this.tabControl);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Championship";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.MainTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainTable)).EndInit();
            this.PlayersStats.ResumeLayout(false);
            this.PlayersStats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlayers)).EndInit();
            this.KeepersStats.ResumeLayout(false);
            this.KeepersStats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKeepers)).EndInit();
            this.GamesResults.ResumeLayout(false);
            this.GamesResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).EndInit();
            this.AddGame.ResumeLayout(false);
            this.AddGame.PerformLayout();
            this.groupBoxAction.ResumeLayout(false);
            this.groupBoxAction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAtGoals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHtGoals)).EndInit();
            this.groupBoxCards.ResumeLayout(false);
            this.groupBoxCards.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxYellow)).EndInit();
            this.groupBoxScore.ResumeLayout(false);
            this.groupBoxScore.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl;
        private TabPage MainTable;
        private TabPage PlayersStats;
        private TabPage KeepersStats;
        private TabPage GamesResults;
        private TabPage AddGame;
        private DataGridView dgvMainTable;
        private Label lblSortBy;
        private ComboBox comBoxPlayersSort;
        private ComboBox comBoxPlayersTeams;
        private DataGridView dgvPlayers;
        private ComboBox comBoxKeepersSort;
        private ComboBox comBoxKeepersTeams;
        private DataGridView dgvKeepers;
        private ComboBox comBoxResults;
        private Label lblKeeper;
        private ComboBox comBoxAtKeeper;
        private ComboBox comBoxHtKeeper;
        private Label lblGoals;
        private Label lblTeams;
        private Label lblInfo;
        private Button buttonSaveGame;
        private GroupBox groupBoxAction;
        private RadioButton radioBtnAt;
        private RadioButton radioBtnHt;
        private CheckBox checkBoxOwnGoal;
        private Button btnSaveAction;
        private CheckBox checkBoxPenOrRed;
        private ComboBox comboBoxPlayersAdditional;
        private ComboBox comboBoxPlayers;
        private Label label2;
        private TextBox textBoxDate;
        private NumericUpDown numAtGoals;
        private NumericUpDown numHtGoals;
        private Button buttonSaveResult;
        private ComboBox comBoxAt;
        private ComboBox comBoxHt;
        private GroupBox groupBoxCards;
        private Label lblAtCardsY;
        private PictureBox pictureBoxRed;
        private PictureBox pictureBoxYellow;
        private Label lblHtCardsR;
        private Label lblAtCardsR;
        private Label lblHtCardsY;
        private GroupBox groupBoxScore;
        private Label lblScoreText;
        private Label lblScore;
        private DataGridView dgvResults;
        private TextBox textBoxPlayersFilter;
        private TextBox textBoxKeepersFilter;
        private Label lblDate;
        private Label lblScorer;
        private Label lblAssistant;
        private Label label1;
        private Label label3;
        private Label label5;
        private Label label6;
        private Label label4;
    }
}