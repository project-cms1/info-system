using System;
using System.Windows.Forms;
using System.Data;
using System.ComponentModel;
//using ClassLib;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Linq;
//using WinFormsLibrary;
using WinFormsLib;

namespace WinFormsApp
{
    [Serializable]
    public partial class MainForm : Form
    {
        DataTable table = new DataTable("table");
        DataTable tablePlayers = new DataTable("tablePlayers");
        DataTable tableKeepers = new DataTable("tableKeepers");
        DataTable tableResults = new DataTable("tableResults");
        Team[] teams;
        Game[,] games;
        public MainForm()
        {
            InitializeComponent();
            teams = Team.LoadItems("TeamsList.bin");
            games = Game.LoadItems("GamesList.bin");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //teams = new Team[8];
            //games = new Game[14, 4];
            SetTables();

            

            FillTeamInitial();
           
            
           
            FillPLayersDgv(0, tablePlayers);
            FillPLayersDgv(comBoxKeepersTeams.SelectedIndex, tableKeepers);


            int[] currentGame = CurrentGame();
            comBoxResultsFiller(currentGame[0],currentGame[1]);
            comBoxResults.SelectedIndex = 0;
            FillMainDgv();
            //FillPLayersDgv(0);
            
            EndOfTournarment();
        }

        private void SetTables()
        {
            table.Columns.Add("Team", Type.GetType("System.String"));
            table.Columns.Add("GP", Type.GetType("System.Int32"));
            table.Columns.Add("Wins", Type.GetType("System.Int32"));
            table.Columns.Add("Draws", Type.GetType("System.Int32"));
            table.Columns.Add("Loses", Type.GetType("System.Int32"));
            table.Columns.Add("Goals scored", Type.GetType("System.Int32"));
            table.Columns.Add("Goals conceded", Type.GetType("System.Int32"));
            table.Columns.Add("Points", Type.GetType("System.Int32"));
            dgvMainTable.DataSource = table;
            dgvMainTable.Columns[7].DefaultCellStyle.Font = new Font("Arial", 13, FontStyle.Bold);
            dgvMainTable.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


            tablePlayers.Columns.Add("Name", Type.GetType("System.String"));
            tablePlayers.Columns.Add("Surname", Type.GetType("System.String"));
            tablePlayers.Columns.Add("Goals", Type.GetType("System.Int32"));
            tablePlayers.Columns.Add("Assists", Type.GetType("System.Int32"));
            tablePlayers.Columns.Add("Yellow cards", Type.GetType("System.Int32"));
            tablePlayers.Columns.Add("Red cards", Type.GetType("System.Int32"));
            tablePlayers.Columns.Add("Own Goals", Type.GetType("System.Int32"));
            tablePlayers.Columns.Add("Penalty Goals", Type.GetType("System.Int32"));
            dgvPlayers.DataSource = tablePlayers;


            tableKeepers.Columns.Add("Name", Type.GetType("System.String"));
            tableKeepers.Columns.Add("Surname", Type.GetType("System.String"));
            tableKeepers.Columns.Add("Clean sheets", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Goals conceded", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Goals", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Assists", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Yellow cards", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Red cards", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Own Goals", Type.GetType("System.Int32"));
            tableKeepers.Columns.Add("Penalty Goals", Type.GetType("System.Int32"));
            dgvKeepers.DataSource = tableKeepers;


            tableResults.Columns.Add("Date", Type.GetType("System.DateTime"));
            tableResults.Columns.Add("Home team", Type.GetType("System.String"));
            tableResults.Columns.Add("Ht goals", Type.GetType("System.Int32"));
            tableResults.Columns.Add(" ", Type.GetType("System.String"));
            tableResults.Columns.Add("At goals", Type.GetType("System.Int32"));
            tableResults.Columns.Add("Away team", Type.GetType("System.String"));
            dgvResults.DataSource = tableResults;
            dgvResults.Columns[1].Width = 140;
            dgvResults.Columns[1].Width = 140;
            dgvResults.Columns[0].Width = 200;
            dgvResults.Columns[3].Width = 50;

            //this.dgvResults.DefaultCellStyle.BackColor = Color.FromArgb(4, 20, 29);
            //this.dgvResults.DefaultCellStyle.ForeColor = Color.FromArgb(190, 190, 190);
            //this.dgvResults.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(50, 50, 240);
            //this.dgvResults.CellBorderStyle = DataGridViewCellBorderStyle.None;
            //this.dgvResults.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Sunken;
            this.dgvResults.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        public void FillTeamInitial()
        {
            //for (int i = 0; i < teams.Length; i++)
              //  teams[i] = new Team();
            FillTeamName();
            teams[comBoxHt.SelectedIndex].ComboBoxKeeperFiller(comBoxHtKeeper);
            teams[comBoxAt.SelectedIndex].ComboBoxKeeperFiller(comBoxAtKeeper);
        }
        public void FillTeamName()
        {
            //string[] a = new string[] {"Canada","Costa Rica","El Salvador","Honduras",
            //"Jamaica","Mexico","Panama","USA"};
            string[] array = new string[teams.Length];
            for (int i = 0; i < teams.Length; i++)
            {
               // teams[i].TeamName = a[i];
                array[i] = teams[i].TeamName;
            }
            FillComboBox(comBoxPlayersTeams, array);
            FillComboBox(comBoxHt, array);
            FillComboBox(comBoxAt, array);
            FillComboBox(comBoxKeepersTeams, array);
            comBoxPlayersSort.SelectedIndex = 0;
            comBoxKeepersSort.SelectedIndex = 0;
            
        }
        
        public void SetGame(Game game)
        {
            int homeTeamId = 0, awayTeamId = 0;
            for (int i = 0; i < teams.Length; i++)
            {
                if (game.HomeTeamName == teams[i].TeamName) homeTeamId = i;
                if (game.AwayTeamName == teams[i].TeamName) awayTeamId = i;
            }

            Team homeTeam = new Team(game.HomeTeamGoals, game.AwayTeamGoals, game.HomeTeamPlayers, game.HomeTeamKeeper);
            FillTeamGame(homeTeam, homeTeamId);
            Team awayTeam = new Team(game.AwayTeamGoals, game.HomeTeamGoals, game.AwayTeamPlayers, game.AwayTeamKeeper);
            FillTeamGame(awayTeam, awayTeamId);
        }
        public void FillTeamGame(Team team, int teamId)
        {
            teams[teamId].GamesPlayed += team.GamesPlayed;
            teams[teamId].TotalWins += team.TotalWins;
            teams[teamId].TotalDraws += team.TotalDraws;
            teams[teamId].TotalLoses += team.TotalLoses;
            teams[teamId].GoalsScored += team.GoalsScored;
            teams[teamId].GoalsConceded += team.GoalsConceded;
            teams[teamId].Points += team.Points;

            for (int i = 0; i < team.FieldPlayers.Length; i++)
            {
                for (int j = 0; j < teams[teamId].FieldPlayers.Length; j++)
                {
                    if (team.FieldPlayers[i].Name == teams[teamId].FieldPlayers[j].Name &&
                        team.FieldPlayers[i].Surname == teams[teamId].FieldPlayers[j].Surname)
                    {
                        teams[teamId].FieldPlayers[j].Goals += team.FieldPlayers[i].Goals;
                        teams[teamId].FieldPlayers[j].Assists += team.FieldPlayers[i].Assists;
                        teams[teamId].FieldPlayers[j].YellowCards += team.FieldPlayers[i].YellowCards;
                        teams[teamId].FieldPlayers[j].RedCards += team.FieldPlayers[i].RedCards;
                        teams[teamId].FieldPlayers[j].OwnGoals += team.FieldPlayers[i].OwnGoals;
                        teams[teamId].FieldPlayers[j].PenaltyGoals += team.FieldPlayers[i].PenaltyGoals;
                    }

                }
            }
            for (int i = 0; i < teams[teamId].GoalKeepers.Length; i++)
            {
                if (team.GoalKeepers[0].Name == teams[teamId].GoalKeepers[i].Name &&
                   team.GoalKeepers[0].Surname == teams[teamId].GoalKeepers[i].Surname)
                {
                    teams[teamId].GoalKeepers[i].Goals += team.GoalKeepers[0].Goals;
                    teams[teamId].GoalKeepers[i].Assists += team.GoalKeepers[0].Assists;
                    if (team.GoalsConceded == 0) teams[teamId].GoalKeepers[0].CleanSheets += 1;
                    teams[teamId].GoalKeepers[i].GoalsConceded += team.GoalsConceded;
                    teams[teamId].GoalKeepers[i].YellowCards += team.GoalKeepers[0].YellowCards;
                    teams[teamId].GoalKeepers[i].RedCards += team.GoalKeepers[0].RedCards;
                    teams[teamId].GoalKeepers[i].OwnGoals += team.GoalKeepers[0].OwnGoals;
                    teams[teamId].GoalKeepers[i].PenaltyGoals += team.GoalKeepers[0].PenaltyGoals;

                }

            }
        }
        public void FillMainDgv()
        {
            ClearDgvRows(dgvMainTable);

            for (int i = 0; i < teams.Length; i++)
                table.Rows.Add(teams[i].TeamName, teams[i].GamesPlayed, teams[i].TotalWins, teams[i].TotalDraws,
                   teams[i].TotalLoses, teams[i].GoalsScored, teams[i].GoalsConceded, teams[i].Points);
            
            SortMainDgv();
        }
        public void SortMainDgv()
        {
            dgvMainTable.Sort(dgvMainTable.Columns["Points"], ListSortDirection.Descending);
           
            for (int i = 1; i < dgvMainTable.RowCount; i++)
            {
                if (Convert.ToInt32(dgvMainTable.Rows[i - 1].Cells[7].Value) == Convert.ToInt32(dgvMainTable.Rows[i].Cells[7].Value))
                {
                    if (Convert.ToInt32(dgvMainTable.Rows[i].Cells[5].Value) - Convert.ToInt32(dgvMainTable.Rows[i].Cells[6].Value) > Convert.ToInt32(dgvMainTable.Rows[i-1].Cells[5].Value) - Convert.ToInt32(dgvMainTable.Rows[i-1].Cells[6].Value))
                    {
                        for (int j = 0; j < dgvMainTable.ColumnCount; j++)
                        {
                            var temp = dgvMainTable.Rows[i - 1].Cells[j].Value;
                            dgvMainTable.Rows[i - 1].Cells[j].Value = dgvMainTable.Rows[i].Cells[j].Value;
                            dgvMainTable.Rows[i].Cells[j].Value = temp;
                        }
                        i--;
                    }
                    else if (Convert.ToInt32(dgvMainTable.Rows[i].Cells[5].Value) - Convert.ToInt32(dgvMainTable.Rows[i].Cells[6].Value) == Convert.ToInt32(dgvMainTable.Rows[i-1].Cells[5].Value) - Convert.ToInt32(dgvMainTable.Rows[i-1].Cells[6].Value))
                    {
                        if ((Convert.ToInt32(dgvMainTable.Rows[i].Cells[5].Value) > Convert.ToInt32(dgvMainTable.Rows[i - 1].Cells[5].Value)))
                        {
                            for (int j = 0; j < dgvMainTable.ColumnCount; j++)
                            {
                                var temp = dgvMainTable.Rows[i - 1].Cells[j].Value;
                                dgvMainTable.Rows[i - 1].Cells[j].Value = dgvMainTable.Rows[i].Cells[j].Value;
                                dgvMainTable.Rows[i].Cells[j].Value = temp;
                            }
                            i--;
                        }
                        else if ((Convert.ToInt32(dgvMainTable.Rows[i].Cells[5].Value) == Convert.ToInt32(dgvMainTable.Rows[i - 1].Cells[5].Value)))
                        {
                            if ((Convert.ToInt32(dgvMainTable.Rows[i].Cells[2].Value) > Convert.ToInt32(dgvMainTable.Rows[i - 1].Cells[2].Value)))
                            {
                                for (int j = 0; j < dgvMainTable.ColumnCount; j++)
                                {
                                    var temp = dgvMainTable.Rows[i - 1].Cells[j].Value;
                                    dgvMainTable.Rows[i - 1].Cells[j].Value = dgvMainTable.Rows[i].Cells[j].Value;
                                    dgvMainTable.Rows[i].Cells[j].Value = temp;
                                }
                                i--;
                            }
                        }
                    }
                }
            }
        }


        //--------------------- Players stats ----------------------------
        private void comBoxPlayersTeams_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPLayersDgv(comBoxPlayersTeams.SelectedIndex, tablePlayers);
        }

       
        public void FillPLayersDgv(int teamId, DataTable dataTable)
        {

            if (dataTable == tableKeepers)
            {
                ClearDgvRows(dgvKeepers);
                for (int i = 0; i < teams[teamId].GoalKeepers.Length; i++)
                    tableKeepers.Rows.Add(teams[teamId].GoalKeepers[i].Name, teams[teamId].GoalKeepers[i].Surname,
                        teams[teamId].GoalKeepers[i].CleanSheets, teams[teamId].GoalKeepers[i].GoalsConceded,
                        teams[teamId].GoalKeepers[i].Goals, teams[teamId].GoalKeepers[i].Assists,
                        teams[teamId].GoalKeepers[i].YellowCards, teams[teamId].GoalKeepers[i].RedCards,
                        teams[teamId].GoalKeepers[i].OwnGoals, teams[teamId].GoalKeepers[i].PenaltyGoals);
            }
            else if (dataTable == tablePlayers)
            {
                ClearDgvRows(dgvPlayers);
                for (int i = 0; i < teams[teamId].FieldPlayers.Length; i++)
                    tablePlayers.Rows.Add(teams[teamId].FieldPlayers[i].Name, teams[teamId].FieldPlayers[i].Surname,
                         teams[teamId].FieldPlayers[i].Goals, teams[teamId].FieldPlayers[i].Assists,
                         teams[teamId].FieldPlayers[i].YellowCards, teams[teamId].FieldPlayers[i].RedCards,
                         teams[teamId].FieldPlayers[i].OwnGoals, teams[teamId].FieldPlayers[i].PenaltyGoals);
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            (dgvPlayers.DataSource as DataTable).DefaultView.RowFilter =
                String.Format("Surname like '%" + textBoxPlayersFilter.Text + "%'");
        }
        private void dgvPlayers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = teams[comBoxPlayersTeams.SelectedIndex].SearchPlayer
                (dgvPlayers.Rows[dgvPlayers.CurrentCell.RowIndex].Cells[0].Value.ToString(), dgvPlayers.Rows[dgvPlayers.CurrentCell.RowIndex].Cells[1].Value.ToString());

            DataSender.Player = teams[comBoxPlayersTeams.SelectedIndex].FieldPlayers[index];
            PlayersForm playersForm = new PlayersForm();
            this.Hide();
            playersForm.ShowDialog();
            this.Show();
        }
        private void comBoxPlayersSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comBoxPlayersSort.SelectedIndex != 0) 
                dgvPlayers.Sort(dgvPlayers.Columns[comBoxPlayersSort.SelectedIndex + 1], ListSortDirection.Descending);
        }
        private void textBoxKeepersFilter_TextChanged(object sender, EventArgs e)
        {
            (dgvKeepers.DataSource as DataTable).DefaultView.RowFilter =
                String.Format("Surname like '%" + textBoxKeepersFilter.Text + "%'");
        }
        private void dgvKeepers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = teams[comBoxKeepersTeams.SelectedIndex].SearchKeeper
                (dgvKeepers.Rows[dgvKeepers.CurrentCell.RowIndex].Cells[0].Value.ToString(), dgvKeepers.Rows[dgvKeepers.CurrentCell.RowIndex].Cells[1].Value.ToString());

            DataSender.Keeper = teams[comBoxKeepersTeams.SelectedIndex].GoalKeepers[index];
            KeeperForm keeperForm = new KeeperForm();
            this.Hide();
            keeperForm.ShowDialog();
            this.Show();
        }
        private void comBoxKeepersSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comBoxKeepersSort.SelectedIndex != 0)
                dgvKeepers.Sort(dgvKeepers.Columns[comBoxKeepersSort.SelectedIndex + 1], ListSortDirection.Descending);
        }
        private void comBoxKeepersTeams_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPLayersDgv(comBoxKeepersTeams.SelectedIndex, tableKeepers);
        }
        //--------------------- Games results ----------------------------
        private void FillDgvResults(int roundID)
        {
            ClearDgvRows(dgvResults);
            for (int i = 0; i < teams.Length / 2; i++)
                if (!String.IsNullOrWhiteSpace(games[roundID, i].HomeTeamName))
                    tableResults.Rows.Add(games[roundID, i].MatchDay, games[roundID, i].HomeTeamName, games[roundID, i].HomeTeamGoals,
                        "VS", games[roundID, i].AwayTeamGoals, games[roundID, i].AwayTeamName);
        }
        private void comboBoxResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDgvResults(comBoxResults.SelectedIndex);
        }
        private void comBoxResultsFiller(int round,int game)
        {
            if (game == 0) round--;
            List<string> array = new List<string>();
            for(int i = 0; i <= round; i++)
            {
                array.Add("Round" + (i + 1).ToString()) ;
            }
            FillComboBox(comBoxResults,array.ToArray<string>());
            comBoxResults.SelectedIndex = round;
        }
        public static void ClearDgvRows(DataGridView dataGridView)
        {
            var dt = dataGridView.DataSource as DataTable;
            if (dt != null)
            {
                //foreach(var column in dt.Columns) column.so
                dt.Rows.Clear();
                dataGridView.DataSource = dt;
            }
        }
        public static void FillComboBox(ComboBox comboBox, string[] array)
        {
            comboBox.Items.Clear();
            foreach (string item in array)
            {
                comboBox.Items.Add(item);
            }
            comboBox.SelectedIndex = 0;
        }
        private void dgvResults_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataSender.SelectedGame = games[comBoxResults.SelectedIndex, Convert.ToInt32(dgvResults.CurrentCell.RowIndex)];
            GameForm gameForm = new GameForm();
            this.Hide();
            gameForm.ShowDialog();
            this.Show();
        }

        //--------------------- Adding games ----------------------------

        private void comBoxHt_SelectedIndexChanged(object sender, EventArgs e)
        {
            teams[comBoxHt.SelectedIndex].ComboBoxKeeperFiller(comBoxHtKeeper);
        }

        private void comBoxAt_SelectedIndexChanged(object sender, EventArgs e)
        {
            teams[comBoxAt.SelectedIndex].ComboBoxKeeperFiller(comBoxAtKeeper);
        }

        GoalKeeper htKeeper = new GoalKeeper();
        GoalKeeper atKeeper = new GoalKeeper();
        private void buttonSaveResult_Click(object sender, EventArgs e)
        {
            int homeIndex = comBoxHt.SelectedIndex, awayIndex = comBoxAt.SelectedIndex;
            Regex regex = new Regex(@"^(?:20[0-1]\d|202[0-2])-(?:(?:(?:01|03|05|07|08|10|12)-(?:(?:0[1-9])|(?:[1-2]\d)|(?:3[0-1])))|(?:(?:04|06|09|11)-(?:(?:0[1-9])|(?:[1-2]\d)|(?:30)))) (?:[0-1]\d|2[0-3]):(?:[0-5]\d)$");
            bool isOkSameTeam = true, isOkMatchTeam = true, isOkRoundTeam = true, isOkDate = true;
            if (comBoxHt.SelectedIndex == comBoxAt.SelectedIndex)
            {
                isOkSameTeam = false;
                MessageBox.Show("Error!", "Teams cannot play each other!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (CheckMatch(teams[comBoxHt.SelectedIndex].TeamName, teams[comBoxAt.SelectedIndex].TeamName))
            {
                isOkMatchTeam = false;
                MessageBox.Show("Error!", "These teams has already played!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (CheckRound(teams[comBoxHt.SelectedIndex].TeamName, teams[comBoxAt.SelectedIndex].TeamName))
            {
                isOkRoundTeam = false;
                MessageBox.Show("Error!", "Teams cannot play twice at the same round!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!regex.IsMatch(textBoxDate.Text))
            {
                isOkDate = false;
                MessageBox.Show("Error!", "Date is incorrect!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (isOkSameTeam && isOkMatchTeam && isOkRoundTeam && isOkDate)
            {
                comBoxAt.Enabled = false; comBoxHt.Enabled = false;
                numHtGoals.Enabled = false; numAtGoals.Enabled = false;
                textBoxDate.Enabled = false; buttonSaveResult.Enabled = false;
                comBoxHtKeeper.Enabled = false; comBoxAtKeeper.Enabled = false;
                

                htKeeper.Name = teams[comBoxHt.SelectedIndex].GoalKeepers[comBoxHtKeeper.SelectedIndex].Name;
                htKeeper.Surname = teams[comBoxHt.SelectedIndex].GoalKeepers[comBoxHtKeeper.SelectedIndex].Surname;
                atKeeper.Name = teams[comBoxAt.SelectedIndex].GoalKeepers[comBoxAtKeeper.SelectedIndex].Name;
                atKeeper.Surname = teams[comBoxAt.SelectedIndex].GoalKeepers[comBoxAtKeeper.SelectedIndex].Surname;

                if ((int)numHtGoals.Value > 0)
                {
                    lblInfo.Text = "Goal actions:";
                    GroupBoxElem(comBoxHt, comBoxHtKeeper);
                    radioBtnHt.Checked = true;
                }
                else if ((int)numAtGoals.Value > 0)
                {
                    lblInfo.Text = "Goal actions:";
                    GroupBoxElem(comBoxAt, comBoxAtKeeper);
                    radioBtnAt.Checked = true;
                }
                else
                {
                    lblInfo.Text = "Card actions:";
                    GroupBoxElem(comBoxHt, comBoxHtKeeper);
                    radioBtnHt.Checked = true;
                    groupBoxCards.Visible = true;
                    buttonSaveGame.Visible = true;
                }
                groupBoxAction.Visible = true;
                lblInfo.Visible = true;
                groupBoxScore.Visible = true;
                lblScore.Text = "0:0";
            }
        }

        List<FieldPlayer> htPlayers = new List<FieldPlayer>();
        List<FieldPlayer> atPlayers = new List<FieldPlayer>();
        static int htGoals = 0;
        static int atGoals = 0;
        private void btnSaveAction_Click(object sender, EventArgs e)
        {
            if (radioBtnHt.Checked && comboBoxPlayers.SelectedIndex + 1 != comboBoxPlayersAdditional.SelectedIndex)
            {
                if (((int)numHtGoals.Value > 0) && !checkBoxOwnGoal.Checked && htGoals != (int)numHtGoals.Value)
                {
                    SetPlayers(htPlayers, htKeeper, comBoxHt.SelectedIndex);
                    htGoals++;
                }
                else if (checkBoxOwnGoal.Checked && atGoals != (int)numAtGoals.Value)
                {
                    SetPlayers(htPlayers, htKeeper, comBoxHt.SelectedIndex);
                    atGoals++;
                }
            }
            else if (radioBtnAt.Checked && comboBoxPlayers.SelectedIndex + 1 != comboBoxPlayersAdditional.SelectedIndex)
            {
                if (((int)numAtGoals.Value > 0) && !checkBoxOwnGoal.Checked && atGoals != (int)numAtGoals.Value)
                {
                    SetPlayers(atPlayers, atKeeper, comBoxAt.SelectedIndex);
                    atGoals++;
                }
                else if (checkBoxOwnGoal.Checked && htGoals != (int)numHtGoals.Value)
                {
                    SetPlayers(atPlayers, atKeeper, comBoxAt.SelectedIndex);
                    htGoals++;
                }
            }

            if ((htGoals == (int)numHtGoals.Value && atGoals == (int)numAtGoals.Value))
            {
                if (lblInfo.Text == "Card actions:")
                {
                    if (radioBtnHt.Checked)
                    {
                        if (comboBoxPlayers.SelectedIndex == 0 && CheckKeepersCards(htKeeper))
                        { SetPlayers(htPlayers, htKeeper, comBoxHt.SelectedIndex); CheckKeeper2YellowCard(htKeeper, lblHtCardsR); }
                        else if (comboBoxPlayers.SelectedIndex != 0 && CheckPlayersCards(htPlayers, teams[comBoxHt.SelectedIndex].FieldPlayers[comboBoxPlayers.SelectedIndex - 1]))
                        { SetPlayers(htPlayers, htKeeper, comBoxHt.SelectedIndex); CheckPlayer2YellowCard(htPlayers, teams[comBoxHt.SelectedIndex].FieldPlayers[comboBoxPlayers.SelectedIndex - 1], lblHtCardsR); }
                    }
                    else if (radioBtnAt.Checked)
                    {
                        if (comboBoxPlayers.SelectedIndex == 0 && CheckKeepersCards(atKeeper))
                        { SetPlayers(atPlayers, atKeeper, comBoxAt.SelectedIndex); CheckKeeper2YellowCard(atKeeper, lblAtCardsR); }
                        else if ((comboBoxPlayers.SelectedIndex != 0 && CheckPlayersCards(atPlayers, teams[comBoxAt.SelectedIndex].FieldPlayers[comboBoxPlayers.SelectedIndex - 1])))
                        { SetPlayers(atPlayers, atKeeper, comBoxAt.SelectedIndex); CheckPlayer2YellowCard(atPlayers, teams[comBoxAt.SelectedIndex].FieldPlayers[comboBoxPlayers.SelectedIndex - 1], lblAtCardsR); }
                    }
                }
                lblInfo.Text = "Card actions:";
                if (Convert.ToInt32(lblHtCardsY.Text) == 0 && Convert.ToInt32(lblHtCardsR.Text) == 0 &&
                Convert.ToInt32(lblAtCardsY.Text) == 0 && Convert.ToInt32(lblAtCardsR.Text) == 0)
                {
                    GroupBoxElem(comBoxHt, comBoxHtKeeper);
                    radioBtnHt.Checked = true;
                    buttonSaveGame.Visible = true;
                }
                groupBoxCards.Visible = true;
            }
            checkBoxPenOrRed.Checked = false; checkBoxOwnGoal.Checked = false;
            comboBoxPlayers.SelectedIndex = 0; comboBoxPlayersAdditional.SelectedIndex = 0;
            lblScore.Text = $"{htGoals}:{atGoals}";
        }
        private void SetPlayers(List<FieldPlayer> players, GoalKeeper keeper, int teamId)
        {

            if (lblInfo.Text == "Goal actions:")
            {
                int goals = 1, pen = 0, owng = 0;
                if (checkBoxPenOrRed.Checked) pen = 1;
                if (checkBoxOwnGoal.Checked) { owng = 1; goals = 0; }

                if (comboBoxPlayers.SelectedIndex == 0)
                {
                    keeper.Goals += goals;
                    keeper.OwnGoals += owng;
                    keeper.PenaltyGoals += pen;
                }
                else
                    players.Add(new FieldPlayer(teams[teamId].FieldPlayers[comboBoxPlayers.SelectedIndex - 1].Name,
                        teams[teamId].FieldPlayers[comboBoxPlayers.SelectedIndex - 1].Surname, goals, 0, 0, 0, owng, pen));

                if (comboBoxPlayersAdditional.SelectedIndex != 0)
                {
                    if (comboBoxPlayersAdditional.SelectedIndex == 1)
                        keeper.Assists += 1;
                    else
                        players.Add(new FieldPlayer(teams[teamId].FieldPlayers[comboBoxPlayersAdditional.SelectedIndex - 2].Name,
                            teams[teamId].FieldPlayers[comboBoxPlayersAdditional.SelectedIndex - 2].Surname, 0, 1, 0, 0, 0, 0));
                }
            }
            else
            {
                int yellow = 0, red = 0;
                if (checkBoxPenOrRed.Checked) { yellow = 0; red = 1; }
                else { yellow = 1; red = 0; }

                if (radioBtnHt.Checked) { lblHtCardsY.Text = Convert.ToString(Convert.ToInt32(lblHtCardsY.Text) + yellow); lblHtCardsR.Text = Convert.ToString(Convert.ToInt32(lblHtCardsR.Text) + red); }
                else { lblAtCardsY.Text = Convert.ToString(Convert.ToInt32(lblAtCardsY.Text) + yellow); lblAtCardsR.Text = Convert.ToString(Convert.ToInt32(lblAtCardsR.Text) + red); }

                if (comboBoxPlayers.SelectedIndex == 0)
                {
                    keeper.YellowCards += yellow;
                    keeper.RedCards += red;
                }
                else
                    players.Add(new FieldPlayer(teams[teamId].FieldPlayers[comboBoxPlayers.SelectedIndex - 1].Name,
                        teams[teamId].FieldPlayers[comboBoxPlayers.SelectedIndex - 1].Surname, 0, 0, yellow, red, 0, 0));
            }
        }
        private bool CheckPlayersCards(List<FieldPlayer> players, FieldPlayer player)
        {
            int yellow = 0, red = 0;
            foreach (var item in players)
            {
                if (item.Name == player.Name && item.Surname == player.Surname)
                {
                    //if (!checkBoxPenOrRed.Checked)
                    yellow += item.YellowCards;
                    //else
                    red += item.RedCards;
                }
            }

            if (red == 1) { lblScore.Text = player.Name; return false; }

            if (!checkBoxPenOrRed.Checked)
            {
                if (yellow == 2)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
        private bool CheckKeepersCards(GoalKeeper keeper)
        {
            if (keeper.YellowCards == 2 || keeper.RedCards == 1)
                return false;
            return true;
        }
        private void CheckPlayer2YellowCard(List<FieldPlayer> players, FieldPlayer player,Label label)
        {
            int yellow = 0; string name="", surname = "";
            foreach (var item in players)
            {
                if (item.Name == player.Name && item.Surname == player.Surname)
                {
                    yellow += item.YellowCards;
                    if (yellow == 2)
                    {
                        name = item.Name;
                        surname = item.Surname;
                    }
                }
            }
            if (name != "")
            {
                players.Add(new FieldPlayer(name, surname, 0, 0, 0, 1, 0, 0));
                label.Text = Convert.ToString(Convert.ToInt32(label.Text) + 1);
            }
        }
        private void CheckKeeper2YellowCard(GoalKeeper keeper,Label label)
        {
            if(keeper.YellowCards==2)
            {
                keeper.RedCards = 1;
                label.Text = Convert.ToString(Convert.ToInt32(label.Text) + 1);  
            }
        }
        private void GroupBoxElem(ComboBox comboBoxTeam, ComboBox comboBoxKeeper)
        {
            radioBtnHt.Text = comBoxHt.Text;
            radioBtnAt.Text = comBoxAt.Text;

            teams[comboBoxTeam.SelectedIndex].ComboBoxPlayersFiller(comboBoxPlayers, comboBoxKeeper.SelectedIndex, false);
            teams[comboBoxTeam.SelectedIndex].ComboBoxPlayersFiller(comboBoxPlayersAdditional, comboBoxKeeper.SelectedIndex, true);

            if (lblInfo.Text == "Goal actions:")
            {
                lblScorer.Text = "Goal scorer:"; lblAssistant.Text = "Assist provider:";
                comboBoxPlayersAdditional.Visible = true; lblAssistant.Visible = true;
                checkBoxPenOrRed.Text = "Penalty";
                checkBoxOwnGoal.Visible = true; checkBoxOwnGoal.Text = "Own goal";
            }
            else if (lblInfo.Text == "Card actions:" && Convert.ToInt32(lblHtCardsY.Text) == 0 && Convert.ToInt32(lblHtCardsR.Text) == 0 &&
                Convert.ToInt32(lblAtCardsY.Text) == 0 && Convert.ToInt32(lblAtCardsR.Text) == 0)
            {
                lblScorer.Text = "Card receiver: ";
                checkBoxPenOrRed.Enabled = true;
                checkBoxPenOrRed.Checked = false;
                checkBoxPenOrRed.Text = "Red card (if not checked - yellow)";
                lblAssistant.Visible = false;
                checkBoxOwnGoal.Visible = false;
                comboBoxPlayersAdditional.Visible = false;
            }
        }
        private void radioBtnHt_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioBtnHt.Checked)
            {
                GroupBoxElem(comBoxAt, comBoxAtKeeper);
                radioBtnAt.Font = new Font("Tahoma", 12, FontStyle.Bold);
                radioBtnHt.Font = new Font("Tahoma", 12, FontStyle.Regular);
            }
            else
            {
                radioBtnHt.Font = new Font("Tahoma", 12, FontStyle.Bold);
                radioBtnAt.Font = new Font("Tahoma", 12, FontStyle.Regular);
                GroupBoxElem(comBoxHt, comBoxHtKeeper);
            }
            checkBoxPenOrRed.Checked = false;
            checkBoxOwnGoal.Checked = false;
        }
        private void checkBoxPenOrRed_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPenOrRed.Checked)
            {
                checkBoxOwnGoal.Enabled = false;
                comboBoxPlayersAdditional.SelectedIndex = 0;
                comboBoxPlayersAdditional.Enabled = false;
            }
            else
            {
                checkBoxOwnGoal.Enabled = true;
                comboBoxPlayersAdditional.Enabled = true;
            }
        }
        private void checkBoxOwnGoal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxOwnGoal.Checked)
            {
                checkBoxPenOrRed.Enabled = false;
                comboBoxPlayersAdditional.SelectedIndex = 0;
                comboBoxPlayersAdditional.Enabled = false;
            }
            else
            {
                checkBoxPenOrRed.Enabled = true;
                comboBoxPlayersAdditional.Enabled = true;
            }
        }
        private void buttonSaveGame_Click(object sender, EventArgs e)
        {
            int[] currentGame = CurrentGame();
            games[currentGame[0], currentGame[1]] = new Game(textBoxDate.Text, comBoxHt.Text, comBoxAt.Text,
                (int)numHtGoals.Value, (int)numAtGoals.Value, teams[comBoxHt.SelectedIndex].Flag, teams[comBoxAt.SelectedIndex].Flag,
                htKeeper, htPlayers.ToArray<FieldPlayer>(), atKeeper, atPlayers.ToArray<FieldPlayer>());


            FillDgvResults(currentGame[0]);
            SetGame(games[currentGame[0], currentGame[1]]);
            //Team.Save(teams);
            //Game.Save(games);
            FillMainDgv();
            FillPLayersDgv(comBoxPlayersTeams.SelectedIndex, tablePlayers);
            FillPLayersDgv(comBoxKeepersTeams.SelectedIndex, tableKeepers);
            MessageBox.Show("Info", "Game succesfully added!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (CurrentGame() != null)
            {
                currentGame = CurrentGame();
                comBoxResultsFiller(currentGame[0], currentGame[1]);
            }
            EndOfTournarment();
            MovetoNextGame();
        }
        private void MovetoNextGame()
        {
            comBoxHt.SelectedIndex = 0; comBoxHt.Enabled = true;
            comBoxAt.SelectedIndex = 0; comBoxAt.Enabled = true;
            comBoxHtKeeper.Enabled = true; comBoxAtKeeper.Enabled = true;
            numHtGoals.Value = 0; numHtGoals.Enabled = true;
            numAtGoals.Value = 0; numAtGoals.Enabled = true;
            textBoxDate.Clear(); textBoxDate.Enabled = true;
            buttonSaveResult.Enabled = true; 

            groupBoxCards.Visible = false; groupBoxScore.Visible = false;
            lblHtCardsY.Text = 0.ToString(); lblHtCardsR.Text = 0.ToString();
            lblAtCardsY.Text = 0.ToString(); lblAtCardsR.Text = 0.ToString();

            lblInfo.Visible = false;
            groupBoxAction.Visible = false;
            buttonSaveGame.Visible = false;

            htGoals = 0; atGoals = 0;
            htPlayers.Clear(); atPlayers.Clear();
            htKeeper = new GoalKeeper(); atKeeper = new GoalKeeper();
        }
        private void EndOfTournarment()
        {
            if (CurrentGame() != null)
            {
                int[] currentGame = CurrentGame();
                label2.Text = $"Round: {currentGame[0] + 1}\nMatch: {currentGame[1] + 1}";
            }
            else if (CurrentGame() == null)
            {
                label2.Visible = false; lblDate.Visible = false;
                lblScore.Visible = false; lblGoals.Visible = false;
                numAtGoals.Visible = false;numHtGoals.Visible = false;
                textBoxDate.Visible = false; comBoxHt.Visible = false;
                comBoxAt.Visible = false;comBoxHtKeeper.Visible = false;
                comBoxAtKeeper.Visible = false;
                Team[] placedTeams = new Team[teams.Length];
             
                for(int i = 0; i < dgvMainTable.RowCount; i++)
                {
                    for (int j = 0; j < teams.Length; j++)
                    {
                        if (teams[j].TeamName == dgvMainTable.Rows[i].Cells[0].Value.ToString())
                        {
                            placedTeams[i] = new Team(teams[j]);
                        }
                    }
                }
                DataSender.Teams = placedTeams;
                //MessageBox.Show("Tournarment has ended", "All games have been played", MessageBoxButtons.OK, MessageBoxIcon.Information);
            End_form endForm = new End_form();
            //this.Hide();
            endForm.ShowDialog();
            //this.Close();
            }
        }
        private bool CheckMatch(string homeTeam, string awayTeam)
        {
            for (int j = 0; j < games.GetLength(0); j++)
                for (int i = 0; i < games.GetLength(1); i++)
                    if (!String.IsNullOrEmpty(games[j, i].HomeTeamName))
                        if (games[j, i].HomeTeamName == homeTeam && games[j, i].AwayTeamName == awayTeam)
                            return true;

            return false;
        }

        private bool CheckRound(string homeTeam, string awayTeam)
        {
            int[] currentGame = CurrentGame();
            for (int i = 0; i < games.GetLength(1); i++)
                if (games[currentGame[0], i].HomeTeamName == homeTeam || games[currentGame[0], i].AwayTeamName == homeTeam ||
                    games[currentGame[0], i].HomeTeamName == awayTeam || games[currentGame[0], i].AwayTeamName == awayTeam)
                    return true;
            return false;
        }

        private int[] CurrentGame()
        {
            for (int j = 0; j < games.GetLength(0); j++)
                for (int i = 0; i < games.GetLength(1); i++)
                    if (String.IsNullOrEmpty(games[j, i].HomeTeamName))
                        return new int[] { j, i };
            return null;
        }

        private void KeepersStats_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Team.Save(teams);
            //Game.Save(games);
        }
    }
}