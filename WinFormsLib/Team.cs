﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Policy;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
namespace WinFormsLib
{
    [Serializable]
    public class Team
    {
        public string TeamName { set; get; }
        public int GamesPlayed { set; get; }
        public int TotalWins { set; get; }
        public int TotalDraws { set; get; }
        public int TotalLoses { set; get; }
        public int GoalsScored { set; get; }
        public int GoalsConceded { set; get; }
        public int Points { set; get; }
        public FieldPlayer[] FieldPlayers { set; get; }
        public GoalKeeper[] GoalKeepers { set; get; }
        public Bitmap Flag { set; get; }

        public Team()
        {
            TeamName = "";
            GamesPlayed = 0;
            TotalWins = 0;
            TotalDraws = 0;
            TotalLoses = 0;
            GoalsScored = 0;
            GoalsConceded = 0;
            Points = 0;
            //Players = new Player[30];
            //for (int i = 0; i < 30; i++)
            //    Players[i] = new Player();

            FieldPlayers = new FieldPlayer[30];
            for (int i = 0; i < 30; i++)
                FieldPlayers[i] = new FieldPlayer();

            GoalKeepers = new GoalKeeper[4];
            for (int i = 0; i < 4; i++)
                GoalKeepers[i] = new GoalKeeper();
        }
        public Team(string[] fields/*,string[] keepers*/)
        {
            TeamName = "";
            GamesPlayed = 0;
            TotalWins = 0;
            TotalDraws = 0;
            TotalLoses = 0;
            GoalsScored = 0;
            GoalsConceded = 0;
            Points = 0;
            FieldPlayers = new FieldPlayer[30];
            for (int i = 0; i < FieldPlayers.Length; i++)
                FieldPlayers[i] = new FieldPlayer();
            /*GoalKeepers = new GoalKeeper[keepers.Length];
            for (int i = 0; i < GoalKeepers.Length; i++)
            {
                string[] name = keepers[i].Split("|");
                GoalKeepers[i].Name = name[1];
                GoalKeepers[i].Surname = name[0];
            }*/
        }
        public Team(int goalsScored, int goalsConceded, FieldPlayer[] fieldPlayers, GoalKeeper goalKeeper)
        {
            GamesPlayed += 1;
            GoalsScored += goalsScored;
            GoalsConceded += goalsConceded;
            if (goalsScored > goalsConceded)
            {
                TotalWins += 1;
                Points += 3;
            }
            else if (goalsScored == goalsConceded)
            {
                TotalDraws += 1;
                Points += 1;
            }
            else
                TotalLoses += 1;

            FieldPlayers = new FieldPlayer[fieldPlayers.Length];
            for (int i = 0; i < FieldPlayers.Length; i++)
                FieldPlayers[i] = new FieldPlayer(fieldPlayers[i]);
            GoalKeepers = new GoalKeeper[1];
            GoalKeepers[0] = new GoalKeeper(goalKeeper);
        }

        public Team(Team team)
        {
            TeamName = team.TeamName;
            GamesPlayed = team.GamesPlayed;
            TotalWins = team.TotalWins;
            TotalDraws = team.TotalDraws;
            TotalLoses = team.TotalLoses;
            GoalsScored = team.GoalsScored;
            GoalsConceded = team.GoalsConceded;
            Points = team.Points;

            FieldPlayers = new FieldPlayer[30];
            for (int i = 0; i < 30; i++)
                FieldPlayers[i] = new FieldPlayer(team.FieldPlayers[i]);

            GoalKeepers = new GoalKeeper[4];
            for (int i = 0; i < 4; i++)
                GoalKeepers[i] = new GoalKeeper(team.GoalKeepers[i]);

            Flag = new Bitmap(team.Flag);
        }
        public int SearchPlayer(string name, string surname)
        {
            for (int i = 0; i < FieldPlayers.Length; i++)
                if (FieldPlayers[i].Name == name && FieldPlayers[i].Surname == surname)
                    return i;
            return -1;
        }
        public int SearchKeeper(string name, string surname)
        {
            for (int i = 0; i < GoalKeepers.Length; i++)
                if (GoalKeepers[i].Name == name && GoalKeepers[i].Surname == surname)
                    return i;
            return -1;
        }
        public int[] GetTopScorers()
        {
            int max=FieldPlayers[0].Goals;
            List<int> index = new List<int>();
            index.Add(0);
            for(int i=1;i<FieldPlayers.Length;i++)
            {
                if (FieldPlayers[i].Goals >= max)
                {
                    if (FieldPlayers[i].Goals > max)
                    {
                        max = FieldPlayers[i].Goals;
                        index.Clear();
                    }
                    index.Add(i);
                }
            }
            return index.ToArray<int>();
        }
        public int[] GetTopAssistant()
        {
            int max = FieldPlayers[0].Assists;
            List<int> index = new List<int>();
            index.Add(0);
            for (int i = 1; i < FieldPlayers.Length; i++)
            {
                if (FieldPlayers[i].Assists >= max)
                {
                    if (FieldPlayers[i].Assists > max)
                    {
                        max = FieldPlayers[i].Assists;
                        index.Clear();
                    }
                    index.Add(i);
                }
            }
            return index.ToArray<int>();
        }
        public int[] GetTopGoalKeeper()
        {
            int max = GoalKeepers[0].CleanSheets;
            List<int> index = new List<int>();
            index.Add(0);
            for (int i = 1; i < GoalKeepers.Length; i++)
            {
                if (GoalKeepers[i].CleanSheets >= max)
                {
                    if (GoalKeepers[i].CleanSheets > max)
                    {
                        max = GoalKeepers[i].CleanSheets;
                        index.Clear();
                    }
                    index.Add(i);
                }
            }
            return index.ToArray<int>();
        }
        public void ComboBoxKeeperFiller(ComboBox comboBox)
        {
            comboBox.Items.Clear();
            for (int i = 0; i < GoalKeepers.Length; i++)
                comboBox.Items.Add(GoalKeepers[i].Name + " " + GoalKeepers[i].Surname);
            comboBox.SelectedIndex = 0;
        }
        public void ComboBoxPlayersFiller(ComboBox comboBox, int keeperId, bool none)
        {
            comboBox.Items.Clear();
            if (none) comboBox.Items.Add("None");
            comboBox.Items.Add(GoalKeepers[keeperId].Name + " " + GoalKeepers[keeperId].Surname);
            for (int i = 0; i < FieldPlayers.Length; i++)
                comboBox.Items.Add(FieldPlayers[i].Name + " " + FieldPlayers[i].Surname);
            comboBox.SelectedIndex = 0;
        }

        public static Team[] LoadItems(string path)
        {
            try
            {
                Team[] teams;
                //deserialize
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    var bformatter = new BinaryFormatter();
                    teams = (Team[])bformatter.Deserialize(stream);
                }
                if (teams[0] is Team)
                    return teams;
                else return null;
            }
            catch
            {
                return null;
            }
        }
        public static void Save(Team[] teams, string customPath = null)
        {
            try
            {
                string dir = Directory.GetCurrentDirectory();
                string serializationFile = Path.Combine(dir, "TeamsList.bin");


                //serialize
                using (Stream stream = File.Open(serializationFile,
                       FileMode.OpenOrCreate))
                {
                    var bformatter = new BinaryFormatter();
                    bformatter.Serialize(stream, teams);
                }
            }
            catch { }
        }
    }
 }