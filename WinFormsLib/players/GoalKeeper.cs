﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Policy;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
namespace WinFormsLib
{
    [Serializable]
    public class GoalKeeper : Player
    {
        public int CleanSheets { set; get; }
        public int GoalsConceded { set; get; }

        public GoalKeeper() : base()
        {
            CleanSheets = 0;
            GoalsConceded = 0;
        }

        public GoalKeeper(string name, string surname, int cleanSheets, int goalsConceded, int goals,
            int assists, int yellowCards, int redCards, int ownGoals, int penaltyGoals)
            : base(name, surname, goals, assists, yellowCards, redCards, ownGoals, penaltyGoals)
        {

        }

        public GoalKeeper(string name, string surname, int goals, int assists,
            int yellowCards, int ownGoals, int penaltyGoals)
            : base(name, surname, goals, assists, yellowCards, ownGoals, penaltyGoals)
        {

        }

        public GoalKeeper(GoalKeeper keeper) 
        {
            Name = keeper.Name;
            Surname = keeper.Surname;
            Goals = keeper.Goals;
            Assists = keeper.Assists;
            YellowCards = keeper.YellowCards;
            RedCards = keeper.RedCards;
            OwnGoals = keeper.OwnGoals;
            PenaltyGoals = keeper.PenaltyGoals;
            CleanSheets = keeper.CleanSheets;
            GoalsConceded = keeper.GoalsConceded;
        }
    }
}
