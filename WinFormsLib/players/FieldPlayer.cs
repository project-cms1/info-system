﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Policy;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
namespace WinFormsLib
{
    [Serializable]
    public class FieldPlayer : Player
    {
        public FieldPlayer() : base()
        { }

        public FieldPlayer(string name, string surname, int goals, int assists,
            int yellowCards, int redCards, int ownGoals, int penaltyGoals)
            : base(name, surname, goals, assists, yellowCards, redCards, ownGoals, penaltyGoals)
        { }

        public FieldPlayer(string name, string surname, int goals, int assists,
            int yellowCards, int ownGoals, int penaltyGoals)
            : base(name, surname, goals, assists, yellowCards, ownGoals, penaltyGoals)
        { }

        public FieldPlayer(FieldPlayer fieldPlayer) : base(fieldPlayer)
        { }
    }

}
