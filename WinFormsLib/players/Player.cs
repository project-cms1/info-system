﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsLib
{
    [Serializable]
    public class Player
    {
        public string Name { set; get; }
        public string Surname { set; get; }
        public int Goals { set; get; }
        public int Assists { set; get; }
        public int YellowCards { set; get; }
        public int RedCards { set; get; }
        public int OwnGoals { set; get; }
        public int PenaltyGoals { set; get; }
        public DateTime DateOfBirth { set; get; }
        public Player()
        {
            Name = "";
            Surname = "";
            Goals = 0;
            Assists = 0;
            YellowCards = 0;
            RedCards = 0;
            OwnGoals = 0;
            PenaltyGoals = 0;
        }
        public Player(string name, string surname, int goals, int assists,
            int yellowCards, int redCards, int ownGoals, int penaltyGoals)
        {
            Name = name;
            Surname = surname;
            Goals = goals;
            Assists = assists;
            YellowCards = yellowCards;
            RedCards = redCards;
            OwnGoals = ownGoals;
            PenaltyGoals = penaltyGoals;
        }
        public Player(string name, string surname, int goals, int assists,
           int yellowCards, int ownGoals, int penaltyGoals)
        {
            Name = name;
            Surname = surname;
            Goals = goals;
            Assists = assists;
            YellowCards = yellowCards;
            if (yellowCards == 2) RedCards = 1;
            OwnGoals = ownGoals;
            PenaltyGoals = penaltyGoals;
        }
        public Player(Player player)
        {
            Name = player.Name;
            Surname = player.Surname;
            Goals = player.Goals;
            Assists = player.Assists;
            YellowCards = player.YellowCards;
            RedCards = player.RedCards;
            OwnGoals = player.OwnGoals;
            PenaltyGoals = player.PenaltyGoals;
        }
    }
}