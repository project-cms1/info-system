﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Policy;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
namespace WinFormsLib
{
    [Serializable]
    public class Game
    {
        public DateTime MatchDay { set; get; }
        public string HomeTeamName { set; get; }
        public string AwayTeamName { set; get; }
        public int HomeTeamGoals { set; get; }
        public int AwayTeamGoals { set; get; }
        public Bitmap HomeTeamFlag { set; get; }
        public Bitmap AwayTeamFlag { set; get; }
        public FieldPlayer[] HomeTeamPlayers { set; get; }
        public FieldPlayer[] AwayTeamPlayers { set; get; }
        public GoalKeeper HomeTeamKeeper { set; get; }
        public GoalKeeper AwayTeamKeeper { set; get; }

        public Game(DateTime matchDay, string homeTeamName, string awayTeamName, int homeTeamGoals, int awayTeamGoals,
            int htPlayersNumber, Player[] homeTeamPlayers, int atPlayersNumber, Player[] awayTeamPlayers)
        {
            MatchDay = matchDay;
            HomeTeamName = homeTeamName;
            AwayTeamName = awayTeamName;
            HomeTeamGoals = homeTeamGoals;
            AwayTeamGoals = awayTeamGoals;
            //HomeTeamPlayers = new Player[homeTeamPlayers.Length];
            //for (int i = 0; i < HomeTeamPlayers.Length; i++)
            //    HomeTeamPlayers[i] = new Player(homeTeamPlayers[i]);
            //AwayTeamPlayers = new Player[awayTeamPlayers.Length];
            //for (int i = 0; i < AwayTeamPlayers.Length; i++)
            //    AwayTeamPlayers[i] = new Player(awayTeamPlayers[i]);
        }
        public Game(DateTime matchDay, string homeTeamName, string awayTeamName, int homeTeamGoals,
            int awayTeamGoals, Bitmap homeTeamFlag, Bitmap awayTeamFlag, GoalKeeper homeTeamKeeper, FieldPlayer[] homeTeamPlayers,
             GoalKeeper awayTeamKeeper, FieldPlayer[] awayTeamPlayers)
        {
            MatchDay = matchDay;
            HomeTeamName = homeTeamName;
            AwayTeamName = awayTeamName;
            HomeTeamGoals = homeTeamGoals;
            AwayTeamGoals = awayTeamGoals;
            HomeTeamFlag = new Bitmap(homeTeamFlag);
            AwayTeamFlag = new Bitmap(awayTeamFlag);

            HomeTeamPlayers = new FieldPlayer[homeTeamPlayers.Length];
            for (int i = 0; i < HomeTeamPlayers.Length; i++)
                HomeTeamPlayers[i] = new FieldPlayer(homeTeamPlayers[i]);
            AwayTeamPlayers = new FieldPlayer[awayTeamPlayers.Length];
            for (int i = 0; i < AwayTeamPlayers.Length; i++)
                AwayTeamPlayers[i] = new FieldPlayer(awayTeamPlayers[i]);

            HomeTeamKeeper = new GoalKeeper(homeTeamKeeper);
            AwayTeamKeeper = new GoalKeeper(awayTeamKeeper);
        }
        public Game(string matchDay, string homeTeamName, string awayTeamName, int homeTeamGoals,
            int awayTeamGoals, Bitmap homeTeamFlag, Bitmap awayTeamFlag, GoalKeeper homeTeamKeeper,
            FieldPlayer[] homeTeamPlayers, GoalKeeper awayTeamKeeper, FieldPlayer[] awayTeamPlayers)
        {
            string[] date = matchDay.Split(" ");
            string[] day = date[0].Split("-");
            string[] hours = date[1].Split(":");
            MatchDay = new DateTime(int.Parse(day[0]), int.Parse(day[1]), int.Parse(day[2]), int.Parse(hours[0]), int.Parse(hours[1]), 0);
            HomeTeamName = homeTeamName;
            AwayTeamName = awayTeamName;
            HomeTeamGoals = homeTeamGoals;
            AwayTeamGoals = awayTeamGoals;
            HomeTeamFlag = new Bitmap(homeTeamFlag);
            AwayTeamFlag = new Bitmap(awayTeamFlag);

            HomeTeamPlayers = new FieldPlayer[homeTeamPlayers.Length];
            for (int i = 0; i < HomeTeamPlayers.Length; i++)
                HomeTeamPlayers[i] = new FieldPlayer(homeTeamPlayers[i]);
            AwayTeamPlayers = new FieldPlayer[awayTeamPlayers.Length];
            for (int i = 0; i < AwayTeamPlayers.Length; i++)
                AwayTeamPlayers[i] = new FieldPlayer(awayTeamPlayers[i]);

            HomeTeamKeeper = new GoalKeeper(homeTeamKeeper);
            AwayTeamKeeper = new GoalKeeper(awayTeamKeeper);
        }
        public Game()
        {
            HomeTeamName = "";
            AwayTeamName = "";
            HomeTeamGoals = 0;
            AwayTeamGoals = 0;
        }
        public string[] GetGoalsScorers(GoalKeeper keeper, FieldPlayer[] players)
        {
            List<string> receivers = new List<string>();
            if (keeper.Goals != 0 && keeper.PenaltyGoals == 0)
            {
                receivers.Add(keeper.Name + " " + keeper.Surname);
                receivers[0] += " (G)";
            }

            foreach (var item in players)
            {
                if (item.Goals != 0 && item.PenaltyGoals == 0)
                {
                    string amount = "";
                    if (item.Goals > 1) amount = $" (X{item.Goals})";
                    string comma = "";
                    if (receivers.ToArray<string>().Length != 0) comma = ", ";
                    receivers.Add(comma + item.Name + " " + item.Surname + amount);
                }
            }
            if (receivers.ToArray<string>().Length == 0) return null;
            return receivers.ToArray<string>();
        }
        public string[] GetAssistsProviders(GoalKeeper keeper, FieldPlayer[] players)
        {
            List<string> receivers = new List<string>();
            if (keeper.Assists != 0)
            {
                receivers.Add(keeper.Name + " " + keeper.Surname);
                receivers[0] += " (G)";
            }

            foreach (var item in players)
            {
                if (item.Assists != 0)
                {
                    string amount = "";
                    if (item.Assists > 1) amount = $" (X{item.Assists})";
                    string comma = "";
                    if (receivers.ToArray<string>().Length != 0) comma = ", ";
                    receivers.Add(comma + item.Name + " " + item.Surname + amount);
                }
            }
            if (receivers.ToArray<string>().Length == 0) return null;
            return receivers.ToArray<string>();
        }
        public string[] GetYellowCardsReceivers(GoalKeeper keeper, FieldPlayer[] players)
        {
            List<string> receivers = new List<string>();
            if (keeper.YellowCards != 0)
            {
                receivers.Add(keeper.Name + " " + keeper.Surname);
                receivers[0] += " (G)";
            }

            foreach (var item in players)
            {
                if (item.YellowCards != 0)
                {
                    string amount = "";
                    if (item.YellowCards > 1) amount = $" (X{item.YellowCards})";
                    string comma = "";
                    if (receivers.ToArray<string>().Length != 0) comma = ", ";
                    receivers.Add(comma + item.Name + " " + item.Surname + amount);
                }
            }
            if (receivers.ToArray<string>().Length == 0) return null;
            return receivers.ToArray<string>();
        }
        public string[] GetRedCardsReceivers(GoalKeeper keeper, FieldPlayer[] players)
        {
            List<string> receivers = new List<string>();
            if (keeper.RedCards != 0)
            {
                receivers.Add(keeper.Name + " " + keeper.Surname);
                receivers[0] += " (G)";
            }

            foreach (var item in players)
            {
                if (item.RedCards != 0)
                {
                    string amount = "";
                    if (item.RedCards > 1) amount = $" (X{item.RedCards})";
                    string comma = "";
                    if (receivers.ToArray<string>().Length != 0) comma = ", ";
                    receivers.Add(comma + item.Name + " " + item.Surname + amount);
                }
            }
            if (receivers.ToArray<string>().Length == 0) return null;
            return receivers.ToArray<string>();
        }
        public string[] GetPenaltyScorers(GoalKeeper keeper, FieldPlayer[] players)
        {
            List<string> receivers = new List<string>();
            if (keeper.PenaltyGoals != 0)
            {
                receivers.Add(keeper.Name + " " + keeper.Surname);
                receivers[0] += " (G)";
            }

            foreach (var item in players)
            {
                if (item.PenaltyGoals != 0)
                {
                    string amount = "";
                    if (item.PenaltyGoals > 1) amount = $" (X{item.PenaltyGoals})";
                    string comma = "";
                    if (receivers.ToArray<string>().Length != 0) comma = ", ";
                    receivers.Add(comma + item.Name + " " + item.Surname + amount);
                }
            }
            if (receivers.ToArray<string>().Length == 0) return null;
            return receivers.ToArray<string>();
        }
        public string[] GetOwnGoalsScorers(GoalKeeper keeper, FieldPlayer[] players)
        {
            List<string> receivers = new List<string>();
            if (keeper.OwnGoals != 0)
            {
                receivers.Add(keeper.Name + " " + keeper.Surname);
                receivers[0] += " (G)";
            }

            foreach (var item in players)
            {
                if (item.OwnGoals != 0)
                {
                    string amount = "";
                    if (item.OwnGoals > 1) amount = $" (X{item.OwnGoals})";
                    string comma = "";
                    if (receivers.ToArray<string>().Length != 0) comma = ", ";
                    receivers.Add(comma + item.Name + " " + item.Surname + amount);
                }
            }
            if (receivers.ToArray<string>().Length == 0) return null;
            return receivers.ToArray<string>();
        }
        public static Game[,] LoadItems(string path)
        {
            try
            {
                Game[,] games;
                //deserialize
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    var bformatter = new BinaryFormatter();
                    games = (Game[,])bformatter.Deserialize(stream);
                }
                if (games[0, 0] is Game)
                    return games;
                else return null;
            }
            catch
            {
                return null;
            }
        }
        public static void Save(Game[,] games, string customPath = null)
        {
            try
            {
                string dir = Directory.GetCurrentDirectory();
                string serializationFile = Path.Combine(dir, "GamesList.bin");

                //List<Team> salesmanList = new List<Team>();
                //salesmanList = items;
                //serialize
                using (Stream stream = File.Open(serializationFile,
                       FileMode.OpenOrCreate))
                {
                    var bformatter = new BinaryFormatter();
                    bformatter.Serialize(stream, games);
                }
            }
            catch { }
        }

    }
}