﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsLib
{
    public static class DataSender
    {
        public static Game SelectedGame { set; get; }
        public static FieldPlayer Player { set; get; }
        public static GoalKeeper Keeper { set; get; }
        public static Team[] Teams { set; get; }
    }
}
